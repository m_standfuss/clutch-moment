package com.clutchmoment.api.controllers;

import com.clutchmoment.api.controllers.requests.JoinGameRequest;
import com.clutchmoment.api.controllers.requests.SetLineupRequest;
import com.clutchmoment.api.controllers.responses.AdvantageResultReply;
import com.clutchmoment.api.controllers.responses.InitiatePitchResponse;
import com.clutchmoment.api.controllers.responses.SwingResultResponse;
import com.clutchmoment.api.controllers.responses.SwingRollResponse;
import com.clutchmoment.api.models.GameIdentification;
import com.clutchmoment.api.models.UserIdentification;
import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.card.Pitcher;
import com.clutchmoment.biz.card.Stadium;
import com.clutchmoment.biz.enums.CardType;
import com.clutchmoment.biz.enums.LineupPosition;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameManager;
import com.clutchmoment.biz.game.Scorecard;
import com.clutchmoment.biz.game.player.BattingOrder;
import com.clutchmoment.biz.game.player.GameCompetitor;
import com.clutchmoment.biz.game.player.Lineup;
import com.clutchmoment.biz.game.player.PlayerDeck;
import com.clutchmoment.data.model.BatterCard;
import com.clutchmoment.data.model.PitcherCard;
import com.clutchmoment.data.model.PlayerCard;
import com.clutchmoment.data.model.StadiumCard;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static com.google.common.base.Preconditions.checkArgument;

@RestController
@ResponseBody
public class GameController {

    private final GameManager gameManager;

    public GameController() {
        gameManager = new GameManager();
    }

    @RequestMapping(value = "createGame", method = RequestMethod.POST)
    public Map<String, Object> startGame() {
        UUID gameKey = gameManager.createGame();
        return Collections.singletonMap("gameKey", gameKey.toString());
    }

    @RequestMapping(value = "joinGame", method = RequestMethod.POST)
    public void joinGame(@RequestBody JoinGameRequest req) {
        UUID gameUUID = getUuidFromRequest(req.getGame());
        gameManager.joinGame(gameUUID, makeCompetitor(req.getUser()));
    }

    @RequestMapping(value = "setLineup", method = RequestMethod.POST)
    public void setLineup(@RequestBody SetLineupRequest req) {
        UUID gameUUID = getUuidFromRequest(req.getGame());
        GameCompetitor competitor = makeCompetitor(req.getUser());
        Lineup lineup = makeLineup();
        gameManager.setLineup(gameUUID, competitor, lineup);
    }

    @RequestMapping(value = "startGame", method = RequestMethod.POST)
    public void startGame(@RequestBody GameIdentification req) {
        UUID gameUUID = getUuidFromRequest(req);
        gameManager.startGame(gameUUID);
    }

    @RequestMapping(value = "initiatePitch", method = RequestMethod.POST)
    public InitiatePitchResponse initiatePitch(@RequestBody GameIdentification req) {
        UUID gameUUID = getUuidFromRequest(req);
        return new InitiatePitchResponse(gameManager.getGame(gameUUID).initiatePitch());
    }

    @RequestMapping(value = "calculateAdvantage", method = RequestMethod.POST)
    public AdvantageResultReply calculateAdvantage(@RequestBody GameIdentification req) {
        UUID gameUUID = getUuidFromRequest(req);
        return new AdvantageResultReply(gameManager.getGame(gameUUID).calculateAdvantage());
    }

    @RequestMapping(value = "initiateSwing", method = RequestMethod.POST)
    public SwingRollResponse initiateSwing(@RequestBody GameIdentification req) {
        UUID gameUUID = getUuidFromRequest(req);
        return new SwingRollResponse(gameManager.getGame(gameUUID).initiateSwing());
    }

    @RequestMapping(value = "finalizeSwingResult", method = RequestMethod.POST)
    public SwingResultResponse finalizeSwingResult(@RequestBody GameIdentification req) {
        UUID gameUUID = getUuidFromRequest(req);
        return new SwingResultResponse(gameManager.getGame(gameUUID).finalizeSwingResult());
    }

    @RequestMapping(value = "finalizeAtBat", method = RequestMethod.POST)
    public void finalizeAtBat(@RequestBody GameIdentification req) {
        UUID gameUUID = getUuidFromRequest(req);
        gameManager.getGame(gameUUID).finalizeAtBat();
    }

    @RequestMapping(value = "scorecard", method = RequestMethod.GET)
    public Scorecard getScorecard(@RequestParam String gameKey) {
        UUID gameUUID = getUuidFromString(gameKey);
        return gameManager.getGame(gameUUID).getScorecard();
    }

    private UUID getUuidFromRequest(GameIdentification request) {
        checkArgument(request != null, "The game identification is required.");
        return getUuidFromString(request.getGameKey());
    }

    private UUID getUuidFromString(String uuid) {
        checkArgument(uuid != null, "The game identification is required.");
        return UUID.fromString(uuid);
    }

    /*
     * Only used until we hook up actual data.
     */
    private GameCompetitor makeCompetitor(UserIdentification userIdentification) {
        checkArgument(userIdentification != null, "The user identification is required.");
        return new GameCompetitor(userIdentification.getUserId(), makePlayerDeck());
    }

    private PlayerDeck makePlayerDeck() {
        return new PlayerDeck(new ArrayList<>(), new ArrayList<>(), makeStadium());
    }

    private Stadium makeStadium() {
        return new Stadium(makeStadiumCard(), makeCard(CardType.STADIUM, "Stadium"));
    }

    private Lineup makeLineup() {
        Lineup lineup = new Lineup();
        List<Batter> batters = new ArrayList<>();
        for (LineupPosition position : LineupPosition.values()) {
            if (position == LineupPosition.PITCHER) {
                lineup.setPlayer(position, makePitcher());
            } else {
                Batter batter = makeBatter(position);
                lineup.setPlayer(position, batter);
                batters.add(batter);
            }
        }
        lineup.setBattingOrder(new BattingOrder(batters));
        return lineup;
    }

    private String getCardPosition(LineupPosition position) {
        switch (position) {
            case CATCHER:
            case DH:
                return "C";
            case CF:
                return "CF";
            case LF:
                return "LF";
            case RF:
                return "RF";
            case SS:
                return "SS";
            case FIRST:
                return "1B";
            case THIRD:
                return "3B";
            case SECOND:
                return "2B";
            case PITCHER:
                return "SP";
        }
        return null;
    }

    private Batter makeBatter(LineupPosition position) {
        return new Batter(makeBatterCard(), makePlayerCard(getCardPosition(position)), makeCard(CardType.BATTER, "Batter"));
    }

    private Pitcher makePitcher() {
        return new Pitcher(makePitcherCard(), makePlayerCard(getCardPosition(LineupPosition.PITCHER)), makeCard(CardType.PITCHER, "Pitcher"));
    }

    private com.clutchmoment.data.model.Card makeCard(CardType cardType, String name) {
        com.clutchmoment.data.model.Card card = new com.clutchmoment.data.model.Card();
        card.setActiveInd(true);
        card.setCardId(1);
        card.setCardNumber(UUID.randomUUID().toString());
        card.setCardType(cardType.name());
        card.setRarity("C");

        String extraName = "";
        for (int i = 0; i < 3; i++) {
            Random r = new Random();
            char c = (char) (r.nextInt(26) + 'a');
            extraName += c;
        }
        card.setName(name + "-" + extraName);
        return card;
    }

    private com.clutchmoment.data.model.PitcherCard makePitcherCard() {
        PitcherCard card = new PitcherCard();
        card.setCommand(5);
        card.setInningLimit(5);
        card.setMistakePitch(2);
        return card;
    }

    private com.clutchmoment.data.model.BatterCard makeBatterCard() {
        BatterCard card = new BatterCard();
        card.setOnBase(14);
        card.setSpeed(5);
        return card;
    }

    private com.clutchmoment.data.model.PlayerCard makePlayerCard(String position) {
        PlayerCard card = new PlayerCard();
        card.setPosition(position);
        card.setTeam(Team.STL.name());
        card.setHandedness("R");
        card.setSalary(100);
        card.setDefense(10);
        card.setClutch(1);

        card.setMatchupHandedness("R");
        card.setMatchupQualifier(1);

        card.setChartStrikeout(3);
        card.setChartGroundball(5);
        card.setChartFlyball(7);
        card.setChartWalk(10);
        card.setChartSingle(13);
        card.setChartDouble(15);
        card.setChartTriple(18);
        return card;
    }

    private StadiumCard makeStadiumCard() {
        StadiumCard card = new StadiumCard();
        card.setEffectClass("com.clutchmoment.biz.game.effect.stadium.ArizonaFieldStadiumEffect");
        return card;
    }
}

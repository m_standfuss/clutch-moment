package com.clutchmoment.api.controllers.requests;

import com.clutchmoment.api.models.GameIdentification;
import com.clutchmoment.api.models.UserIdentification;

public class JoinGameRequest {

    private GameIdentification game;
    private UserIdentification user;

    public GameIdentification getGame() {
        return game;
    }

    public void setGame(GameIdentification game) {
        this.game = game;
    }

    public UserIdentification getUser() {
        return user;
    }

    public void setUser(UserIdentification user) {
        this.user = user;
    }
}

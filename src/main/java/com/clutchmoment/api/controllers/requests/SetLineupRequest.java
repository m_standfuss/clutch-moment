package com.clutchmoment.api.controllers.requests;

import com.clutchmoment.api.models.CardIdentification;
import com.clutchmoment.api.models.GameIdentification;
import com.clutchmoment.api.models.UserIdentification;

import java.util.List;

public class SetLineupRequest {

    private GameIdentification game;
    private UserIdentification user;

    private List<CardIdentification> lineupCards;

    public GameIdentification getGame() {
        return game;
    }

    public void setGame(GameIdentification game) {
        this.game = game;
    }

    public UserIdentification getUser() {
        return user;
    }

    public void setUser(UserIdentification user) {
        this.user = user;
    }

    public List<CardIdentification> getLineupCards() {
        return lineupCards;
    }

    public void setLineupCards(List<CardIdentification> lineupCards) {
        this.lineupCards = lineupCards;
    }
}

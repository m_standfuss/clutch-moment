package com.clutchmoment.api.controllers.responses;

import com.clutchmoment.biz.game.AdvantageResult;

public class AdvantageResultReply {

    private final AdvantageResult result;

    public AdvantageResultReply(AdvantageResult result) {
        this.result = result;
    }

    public AdvantageResult getResult() {
        return result;
    }
}

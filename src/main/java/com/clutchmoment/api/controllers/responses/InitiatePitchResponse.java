package com.clutchmoment.api.controllers.responses;

public class InitiatePitchResponse {

    private final int pitchRoll;

    public InitiatePitchResponse(int pitchRoll){
        this.pitchRoll = pitchRoll;
    }

    public int getPitchRoll() {
        return pitchRoll;
    }
}

package com.clutchmoment.api.controllers.responses;

import com.clutchmoment.biz.game.SwingResult;

public class SwingResultResponse {

    private final SwingResult swingResult;

    public SwingResultResponse(SwingResult swingResult) {
        this.swingResult = swingResult;
    }

    public SwingResult getSwingResult() {
        return swingResult;
    }
}

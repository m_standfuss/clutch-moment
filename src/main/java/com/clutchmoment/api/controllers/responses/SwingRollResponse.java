package com.clutchmoment.api.controllers.responses;

public class SwingRollResponse {

    private final int swingRoll;

    public SwingRollResponse(int swingRoll) {
        this.swingRoll = swingRoll;
    }

    public int getSwingRoll() {
        return swingRoll;
    }
}

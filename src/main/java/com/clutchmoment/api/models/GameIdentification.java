package com.clutchmoment.api.models;

public class GameIdentification {

	private String gameKey;

	public String getGameKey() {
		return gameKey;
	}

	public void setGameKey(String gameKey) {
		this.gameKey = gameKey;
	}

}

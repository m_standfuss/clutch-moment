package com.clutchmoment.biz;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.clutchmoment.data.model.DataEntity;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * A map that automatically handles the mapping of the primary key to the entity
 * object. Provides a wrapper for the common HashMap that can handle
 * automatically loading any object that extends the OfficeManagerEntity
 * interface.
 *
 * @param <T> The type of entity to store on the map.
 */
public class EntityMap<T extends DataEntity> implements Iterable<T> {

	private Map<Serializable, T> entityMap;

	/**
	 * Creates a new entity map instance.
	 *
	 * @return A new empty entity map.
	 */
	public static <T extends DataEntity> EntityMap<T> newEntityMap() {
		return new EntityMap<>();
	}

	/**
	 * Create a new entity map instance and populates it with the collection of
	 * entities.
	 *
	 * @param entities The entities to populate the initial map with.
	 * @return A new pre-populated entity map.
	 * @throws IllegalArgumentException If the list is null or contains null.
	 */
	public static <T extends DataEntity> EntityMap<T> newEntityMap(Collection<T> entities) {
		checkArgument(entities != null, "The entities cannot be null.");
		checkArgument(!entities.contains(null), "The entities cannot contain a null value.");
		return new EntityMap<>(entities);
	}

	/**
	 * Creates a new empty entity map.
	 */
	public EntityMap() {
		entityMap = Maps.newHashMap();
	}

	/**
	 * Creates a new entity map and populates it with the collection of entities by
	 * calling the loadMap method.
	 *
	 * @param entities The entities to populate the map with
	 * @throws IllegalArgumentException If the list is null or contains null.
	 */
	public EntityMap(Collection<T> entities) {
		this();
		load(entities);
	}

	/**
	 * Adds a single entity to the entity map.
	 *
	 * @param entity The entity to add.
	 */
	public void add(T entity) {
		checkArgument(entity != null, "The entity cannot be null.");
		entityMap.put(entity.getPrimaryKey(), entity);
	}

	/**
	 * Populates the map with the supplied entities.
	 *
	 * @param entities The entities to populate the map with
	 * @throws IllegalArgumentException If the list is null or contains null.
	 */
	public void load(Collection<T> entities) {
		checkArgument(entities != null, "The entities cannot be null.");
		checkArgument(!entities.contains(null), "The entities cannot contain a null value.");
		for (final T entity : entities) {
			entityMap.put(entity.getPrimaryKey(), entity);
		}
	}

	/**
	 * Returns the mapped to entity for the id, null if none is found.
	 *
	 * @param id The identifier of the entity looking to get.
	 * @return The entity if one such exists, null if not.
	 */
	public T get(Serializable id) {
		return entityMap.get(id);
	}

	/**
	 * Removes all of the entries of the map. The map will be empty after this call
	 * returns.
	 */
	public void clear() {
		entityMap.clear();
	}

	/**
	 * Gets the current size of the map.
	 *
	 * @return The size of the map.
	 */
	public int size() {
		return entityMap.size();
	}

	/**
	 * Gets the collection representation of the entity map's values.
	 *
	 * @return The collection of values.
	 */
	public Collection<T> convertToCollection() {
		return Lists.newArrayList(entityMap.values());
	}

	@Override
	public String toString() {
		return "EntityMap [entityMap=" + entityMap + "]";
	}

	@Override
	public Iterator<T> iterator() {
		return entityMap.values().iterator();
	}
}
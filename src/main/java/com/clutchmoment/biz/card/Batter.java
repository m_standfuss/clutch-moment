package com.clutchmoment.biz.card;

import com.clutchmoment.data.model.BatterCard;
import com.clutchmoment.data.model.Card;
import com.clutchmoment.data.model.PlayerCard;

public class Batter extends Player {

	private final int onBase;
	private final int speed;

	public Batter(BatterCard batterCard, PlayerCard playerCard, Card card) {
		super(playerCard, card);
		this.onBase = batterCard.getOnBase();
		this.speed = batterCard.getSpeed();
	}

	public Integer getOnBase() {
		return onBase;
	}

	public Integer getSpeed() {
		return speed;
	}
}

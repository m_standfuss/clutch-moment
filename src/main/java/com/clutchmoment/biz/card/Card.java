package com.clutchmoment.biz.card;

import static com.google.common.base.Preconditions.checkArgument;

import com.clutchmoment.biz.enums.CardType;
import com.clutchmoment.biz.enums.Rarity;

public class Card {

	private final long cardId;
	private final String name;
	private final String cardNumber;
	private final Rarity rarity;
	private final CardType cardType;

	public Card(com.clutchmoment.data.model.Card card) {
		checkArgument(card != null, "The card model cannot be null.");
		this.cardId = card.getCardId();
		this.name = card.getName();
		this.cardNumber = card.getCardNumber();
		this.rarity = Rarity.toEnum(card.getRarity());
		this.cardType = CardType.toEnum(card.getCardType());
	}

	public Long getCardId() {
		return cardId;
	}

	public String getName() {
		return name;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public Rarity getRarity() {
		return rarity;
	}

	public CardType getCardType() {
		return cardType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cardNumber == null) ? 0 : cardNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (cardNumber == null) {
			if (other.cardNumber != null)
				return false;
		} else if (!cardNumber.equals(other.cardNumber))
			return false;
		return true;
	}
}

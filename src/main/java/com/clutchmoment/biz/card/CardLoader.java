package com.clutchmoment.biz.card;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.clutchmoment.biz.EntityMap;
import com.clutchmoment.biz.enums.CardType;
import com.clutchmoment.data.access.BatterCardDao;
import com.clutchmoment.data.access.CardDao;
import com.clutchmoment.data.access.PitcherCardDao;
import com.clutchmoment.data.access.PlayerCardDao;
import com.clutchmoment.data.access.StadiumCardDao;
import com.clutchmoment.data.access.StrategyCardDao;
import com.clutchmoment.data.model.BatterCard;
import com.clutchmoment.data.model.PitcherCard;
import com.clutchmoment.data.model.PlayerCard;
import com.clutchmoment.data.model.StadiumCard;
import com.clutchmoment.data.model.StrategyCard;

public class CardLoader {

	private static final Logger logger = LogManager.getLogger(CardLoader.class);

	private final CardDao cardDao;
	private final BatterCardDao batterCardDao;
	private final PlayerCardDao playerCardDao;
	private final PitcherCardDao pitcherCardDao;
	private final StadiumCardDao stadiumCardDao;
	private final StrategyCardDao strategyCardDao;

	public CardLoader() {
		cardDao = new CardDao();
		batterCardDao = new BatterCardDao();
		playerCardDao = new PlayerCardDao();
		pitcherCardDao = new PitcherCardDao();
		stadiumCardDao = new StadiumCardDao();
		strategyCardDao = new StrategyCardDao();
	}

	public List<Card> loadCards(List<Long> cardIds) {
		checkArgument(cardIds != null, "The card ids cannot be null.");
		logger.entry(Arrays.toString(cardIds.toArray()));
		List<com.clutchmoment.data.model.Card> cardModels = cardDao.findByIds(cardIds);
		List<Long> playerCardIds = new ArrayList<>();
		List<Long> batterCardIds = new ArrayList<>();
		List<Long> pitcherCardIds = new ArrayList<>();
		List<Long> stadiumCardIds = new ArrayList<>();
		List<Long> strategyCardIds = new ArrayList<>();
		for (com.clutchmoment.data.model.Card cardModel : cardModels) {
			CardType cardType;
			try {
				cardType = CardType.toEnum(cardModel.getCardType());
			} catch (IllegalArgumentException e) {
				logger.error("The card type '" + cardModel.getCardType() + "' was not recognized.", e);
				continue;
			}
			switch (cardType) {
			case BATTER:
				batterCardIds.add(cardModel.getCardId());
				playerCardIds.add(cardModel.getCardId());
				break;
			case PITCHER:
				pitcherCardIds.add(cardModel.getCardId());
				playerCardIds.add(cardModel.getCardId());
				break;
			case STADIUM:
				stadiumCardIds.add(cardModel.getCardId());
				break;
			case STRATEGY:
				strategyCardIds.add(cardModel.getCardId());
				break;
			default:
				logger.error("The card type was not supported for converting to a card object.");
				continue;
			}
		}

		EntityMap<BatterCard> batterCards = new EntityMap<BatterCard>(batterCardDao.findByIds(batterCardIds));
		EntityMap<PlayerCard> playerCards = new EntityMap<PlayerCard>(playerCardDao.findByIds(playerCardIds));
		EntityMap<PitcherCard> pitcherCards = new EntityMap<PitcherCard>(pitcherCardDao.findByIds(pitcherCardIds));
		EntityMap<StadiumCard> stadiumCards = new EntityMap<StadiumCard>(stadiumCardDao.findByIds(stadiumCardIds));
		EntityMap<StrategyCard> strategyCards = new EntityMap<StrategyCard>(strategyCardDao.findByIds(strategyCardIds));

		List<Card> cards = new ArrayList<>(cardModels.size());
		for (com.clutchmoment.data.model.Card cardModel : cardModels) {
			CardType cardType;
			try {
				cardType = CardType.toEnum(cardModel.getCardType());
			} catch (IllegalArgumentException e) {
				logger.error("The card type '" + cardModel.getCardType() + "' was not recognized.", e);
				continue;
			}
			switch (cardType) {
			case BATTER:
				BatterCard batterCard = batterCards.get(cardModel.getCardId());
				PlayerCard playerCard = playerCards.get(cardModel.getCardId());
				cards.add(new Batter(batterCard, playerCard, cardModel));
				break;
			case PITCHER:
				PitcherCard pitcherCard = pitcherCards.get(cardModel.getCardId());
				PlayerCard playerPitcherCard = playerCards.get(cardModel.getCardId());
				cards.add(new Pitcher(pitcherCard, playerPitcherCard, cardModel));
				break;
			case STADIUM:
				StadiumCard stadiumCard = stadiumCards.get(cardModel.getCardId());
				cards.add(new Stadium(stadiumCard, cardModel));
				break;
			case STRATEGY:
				StrategyCard strategyCard = strategyCards.get(cardModel.getCardId());
				cards.add(new Strategy(strategyCard, cardModel));
				break;
			default:
				logger.error("The card type was not supported for converting to a card object.");
				continue;
			}
		}
		return cards;
	}

}

package com.clutchmoment.biz.card;

import com.clutchmoment.data.model.Card;
import com.clutchmoment.data.model.PitcherCard;
import com.clutchmoment.data.model.PlayerCard;

public class Pitcher extends Player {

	private final int command;
	private final int inningsLimit;
	private final int mistakePitchMax;

	public Pitcher(PitcherCard pitcherCard, PlayerCard playerCard, Card card) {
		super(playerCard, card);
		this.command = pitcherCard.getCommand();
		this.inningsLimit = pitcherCard.getInningLimit();
		this.mistakePitchMax = pitcherCard.getMistakePitch();
	}

	public Integer getCommand() {
		return command;
	}

	public Integer getInningsLimit() {
		return inningsLimit;
	}

	public Integer getMistakePitchMax() {
		return mistakePitchMax;
	}
}

package com.clutchmoment.biz.card;

import java.util.ArrayList;
import java.util.List;

import com.clutchmoment.biz.enums.Handedness;
import com.clutchmoment.biz.enums.Icon;
import com.clutchmoment.biz.enums.CardPosition;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.data.model.PlayerCard;

public class Player extends Card {

	private final CardPosition position;
	private final Team team;
	private final Handedness handedness;
	private final int salary;
	private final int defense;
	private final int clutch;
	private final Handedness matchupHandedness;
	private final int matchupQualifier;

	private final int resultStrikeout;
	private final int resultGroundball;
	private final int resultFlyball;
	private final int resultWalk;
	private final int resultSingle;
	private final int resultDouble;
	private final int resultTriple;

	private List<Icon> icons;

	public Player(PlayerCard playerCard, com.clutchmoment.data.model.Card card) {
		super(card);
		this.position = CardPosition.toEnum(playerCard.getPosition());
		this.team = Team.toEnum(playerCard.getTeam());
		this.handedness = Handedness.toEnum(playerCard.getHandedness());
		this.salary = playerCard.getSalary();
		this.defense = playerCard.getDefense();
		this.clutch = playerCard.getClutch();
		this.matchupHandedness = Handedness.toEnum(playerCard.getMatchupHandedness());
		this.matchupQualifier = playerCard.getMatchupQualifier();

		this.resultStrikeout = playerCard.getChartStrikeout();
		this.resultGroundball = playerCard.getChartGroundball();
		this.resultFlyball = playerCard.getChartFlyball();
		this.resultWalk = playerCard.getChartWalk();
		this.resultSingle = playerCard.getChartSingle();
		this.resultDouble = playerCard.getChartDouble();
		this.resultTriple = playerCard.getChartTriple();

		this.icons = new ArrayList<>();
	}

	public CardPosition getPosition() {
		return position;
	}

	public Team getTeam() {
		return team;
	}

	public Handedness getHandedness() {
		return handedness;
	}

	public int getSalary() {
		return salary;
	}

	public int getDefense() {
		return defense;
	}

	public int getClutch() {
		return clutch;
	}

	public List<Icon> getIcons() {
		return icons;
	}

	public Handedness getMatchupHandedness() {
		return matchupHandedness;
	}

	public int getMatchupQualifier() {
		return matchupQualifier;
	}

	public int getResultStrikeout() {
		return resultStrikeout;
	}

	public int getResultGroundball() {
		return resultGroundball;
	}

	public int getResultFlyball() {
		return resultFlyball;
	}

	public int getResultWalk() {
		return resultWalk;
	}

	public int getResultSingle() {
		return resultSingle;
	}

	public int getResultDouble() {
		return resultDouble;
	}

	public int getResultTriple() {
		return resultTriple;
	}
}

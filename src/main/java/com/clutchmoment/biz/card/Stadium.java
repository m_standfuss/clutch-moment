package com.clutchmoment.biz.card;

import com.clutchmoment.data.model.StadiumCard;

public class Stadium extends Card {

	private final int leftFieldDimension;
	private final int centerFieldDimension;
	private final int rightFieldDimension;

	private final boolean indoor;
	private final String effectClass;

	public Stadium(StadiumCard stadiumCard, com.clutchmoment.data.model.Card card) {
		super(card);
		this.leftFieldDimension = stadiumCard.getLeftFieldDimension();
		this.centerFieldDimension = stadiumCard.getCenterFieldDimension();
		this.rightFieldDimension = stadiumCard.getRightFieldDimension();
		this.indoor = stadiumCard.isIndoorInd();
		this.effectClass = stadiumCard.getEffectClass();
	}

	public int getLeftFieldDimension() {
		return leftFieldDimension;
	}

	public int getCenterFieldDimension() {
		return centerFieldDimension;
	}

	public int getRightFieldDimension() {
		return rightFieldDimension;
	}

	public boolean isIndoor() {
		return indoor;
	}

	public String getEffectClass() {
		return effectClass;
	}
}

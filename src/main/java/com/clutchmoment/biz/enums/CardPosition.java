package com.clutchmoment.biz.enums;

public enum CardPosition {
	CATCHER, STARTING_PITCHER, RELIEF_PITCHER, CLOSING_PITCHER, FIRST_BASEMEN, SECOND_BASEMEN, THIRD_BASEMEN, SHORTSTOP,
	LEFT_FIELDER, CENTER_FIELDER, RIGHT_FIELDER;
	/**
	 * Maps a string representation of the position to an enumeration.
	 * 
	 * @param position The string representation.
	 * @return The enumeration
	 * @throws IllegalArumentException If we cannot map the string to an enumeration
	 *                                 properly.
	 */
	public static CardPosition toEnum(String position) {
		switch (position) {
		case "C":
			return CATCHER;
		case "SP":
			return STARTING_PITCHER;
		case "RP":
			return RELIEF_PITCHER;
		case "CP":
			return CLOSING_PITCHER;
		case "1B":
			return FIRST_BASEMEN;
		case "2B":
			return SECOND_BASEMEN;
		case "3B":
			return THIRD_BASEMEN;
		case "SS":
			return SHORTSTOP;
		case "LF":
			return LEFT_FIELDER;
		case "CF":
			return CENTER_FIELDER;
		case "RF":
			return RIGHT_FIELDER;
		default:
			throw new IllegalArgumentException("Could not map the position '" + position + "' to an enumeration.");
		}
	}

	public boolean isInfieldPosition() {
		return this == FIRST_BASEMEN || this == SECOND_BASEMEN || this == SHORTSTOP || this == THIRD_BASEMEN;
	}

	public boolean isOutfieldPosition() {
		return this == LEFT_FIELDER || this == RIGHT_FIELDER || this == CENTER_FIELDER;
	}

	public boolean isPitcherPosition() {
		return this == STARTING_PITCHER || this == RELIEF_PITCHER || this == CLOSING_PITCHER;
	}

	public boolean isBatteryPosition() {
		return isPitcherPosition() || this == CATCHER;
	}
}

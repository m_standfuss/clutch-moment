package com.clutchmoment.biz.enums;

public enum CardType {
	BATTER, PITCHER, STADIUM, STRATEGY;

	/**
	 * Converts a string representation of a card type to the enumeration value.
	 * 
	 * @param cardType The string representation.
	 * @return The enumeration value.
	 * @throws IllegalArgumentException If the card type is not recognized.
	 */
	public static CardType toEnum(String cardType) {
		return CardType.valueOf(cardType);
	}
}

package com.clutchmoment.biz.enums;

public enum Handedness {
	RIGHT, LEFT, SWITCH;

	/**
	 * Maps a string representation of the handedness to an enumeration.
	 * 
	 * @param handedness The string representation.
	 * @return The enumeration
	 * @throws IllegalArumentException If we cannot map the string to an enumeration
	 *                                 properly.
	 */
	public static Handedness toEnum(String handedness) {
		switch (handedness) {
		case "R":
			return RIGHT;
		case "L":
			return LEFT;
		case "S":
			return SWITCH;
		default:
			throw new IllegalArgumentException("Could not map the handedness '" + handedness + "' to an enumeration.");
		}
	}
}

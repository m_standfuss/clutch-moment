package com.clutchmoment.biz.enums;

import com.clutchmoment.biz.game.player.Lineup;

import static com.google.common.base.Preconditions.checkArgument;

public enum LineupPosition {
	PITCHER, CATCHER, FIRST, SECOND, THIRD, SS, RF, CF, LF, DH;

	public LineupPosition fromCardPosition(CardPosition cardPosition){
		checkArgument(cardPosition != null, "The card position cannot be null.");
		switch (cardPosition){
			case CATCHER:
				return LineupPosition.CATCHER;
			case SHORTSTOP:
				return LineupPosition.SS;
			case LEFT_FIELDER:
				return LineupPosition.LF;
			case FIRST_BASEMEN:
				return LineupPosition.FIRST;
			case RIGHT_FIELDER:
				return LineupPosition.RF;
			case THIRD_BASEMEN:
				return LineupPosition.THIRD;
			case CENTER_FIELDER:
				return LineupPosition.CF;
			case RELIEF_PITCHER:
				return LineupPosition.PITCHER;
			case SECOND_BASEMEN:
				return LineupPosition.SECOND;
			case CLOSING_PITCHER:
				return LineupPosition.PITCHER;
			case STARTING_PITCHER:
				return LineupPosition.PITCHER;
		}
		throw new IllegalArgumentException("The provided card position doesnt map to a lineup position.");
	}
}
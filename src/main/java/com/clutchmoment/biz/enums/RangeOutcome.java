package com.clutchmoment.biz.enums;

/*
 * These enumerations must retain the order in which they are sorted in, ie K
 * before GB, before FB...
 */
public enum RangeOutcome {
	STRIKEOUT, GROUNDBALL, FLYBALL, WALK, SINGLE, DOUBLE, TRIPLE, HOMERUN;

	public boolean isOut() {
		return this == STRIKEOUT || this == GROUNDBALL || this == FLYBALL;
	}
}
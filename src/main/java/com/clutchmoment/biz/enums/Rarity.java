package com.clutchmoment.biz.enums;

import org.apache.logging.log4j.LogManager;

public enum Rarity {
	COMMON, UNCOMMON, RARE, ULTRA_RARE;

	public static Rarity toEnum(String rarity) {
		switch (rarity) {
		case "C":
			return COMMON;
		case "UC":
			return RARE;
		case "R":
			return ULTRA_RARE;
		case "UR":
		default:
			LogManager.getLogger(Rarity.class).error("Unable to map '" + rarity + "' to an enumeration.");
			return COMMON;
		}
	}
}

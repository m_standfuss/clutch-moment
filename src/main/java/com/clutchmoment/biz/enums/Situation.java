package com.clutchmoment.biz.enums;

public enum Situation {
	OFFENSE, DEFENSE, NEUTRAL, MANAGER, WEATHER
}

package com.clutchmoment.biz.enums;

import org.apache.logging.log4j.LogManager;

public enum Team {
	ARZ, ATL, BAL, BOS, CHI_A, CHI_N, CIN, CLE, COL, DET, HOU, KC, LA_A, LA_N, MIA, MIL, MIN, NY_A, NY_N, OAK, PHI, PIT,
	SEA, SND, SNF, STL, TEX, TOR, TPB, WAS, UNK;

	public static Team toEnum(String team) {
		try {
			return Team.valueOf(team);
		} catch (IllegalArgumentException e) {
			LogManager.getLogger(Team.class).error("Unable to map '" + team + "' to an enumeration.");
			return UNK;
		}
	}
}

package com.clutchmoment.biz.game;

public class AdvanceRunnersRequest {

	private final boolean advanceRunnerOnFirst;
	private final boolean advanceRunnerOnSecond;
	private final boolean advanceRunnerOnThird;

	public AdvanceRunnersRequest(boolean advanceRunnerOnFirst, boolean advanceRunnerOnSecond,
			boolean advanceRunnerOnThird) {
		this.advanceRunnerOnFirst = advanceRunnerOnFirst;
		this.advanceRunnerOnSecond = advanceRunnerOnSecond;
		this.advanceRunnerOnThird = advanceRunnerOnThird;
	}

	public boolean isAdvanceRunnerOnFirst() {
		return advanceRunnerOnFirst;
	}

	public boolean isAdvanceRunnerOnSecond() {
		return advanceRunnerOnSecond;
	}

	public boolean isAdvanceRunnerOnThird() {
		return advanceRunnerOnThird;
	}
}

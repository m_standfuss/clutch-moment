package com.clutchmoment.biz.game;

public class Advantage {

	private final boolean pitcherAdvantage;
	private final int dieRollOutcome;

	public Advantage(boolean pitcherAdvantage, int dieRollOutcome) {
		this.pitcherAdvantage = pitcherAdvantage;
		this.dieRollOutcome = dieRollOutcome;
	}

	public boolean isPitcherAdvantage() {
		return pitcherAdvantage;
	}

	public int getDieRollOutcome() {
		return dieRollOutcome;
	}

}

package com.clutchmoment.biz.game;

public class AdvantageResult {

    private final boolean pitcherAdvantage;
    private final boolean mistakePitch;
    private final int dieRollOutcome;
    private final int pitcherCommand;
    private final int batterOnBase;

    public AdvantageResult(boolean pitcherAdvantage, boolean mistakePitch, int dieRollOutcome, int pitcherCommand, int batterOnBase) {
        this.pitcherAdvantage = pitcherAdvantage;
        this.mistakePitch = mistakePitch;
        this.dieRollOutcome = dieRollOutcome;
        this.pitcherCommand = pitcherCommand;
        this.batterOnBase = batterOnBase;
    }

    public boolean isPitcherAdvantage() {
        return pitcherAdvantage;
    }

    public int getDieRollOutcome() {
        return dieRollOutcome;
    }

    public boolean isMistakePitch() {
        return mistakePitch;
    }

    public int getPitcherCommand() {
        return pitcherCommand;
    }

    public int getBatterOnBase() {
        return batterOnBase;
    }
}

package com.clutchmoment.biz.game;

import static com.google.common.base.Preconditions.checkArgument;

import com.clutchmoment.biz.enums.Base;

public class DefensiveThrow {

	private final Base baseThrownTo;

	public DefensiveThrow(Base baseThrownTo) {
		checkArgument(baseThrownTo != null, "The base to throw to cannot be null.");
		checkArgument(baseThrownTo != Base.FIRST, "A defensive throw cannot be made to first base.");
		this.baseThrownTo = baseThrownTo;
	}

	public boolean isThrowToSecond() {
		return baseThrownTo == Base.SECOND;
	}

	public boolean isThrowToThird() {
		return baseThrownTo == Base.THIRD;
	}

	public boolean isThrowHome() {
		return baseThrownTo == Base.HOME;
	}
}

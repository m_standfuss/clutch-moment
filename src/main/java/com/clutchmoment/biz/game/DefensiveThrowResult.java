package com.clutchmoment.biz.game;

public class DefensiveThrowResult {
	private final DefensiveThrow defensiveThrow;
	private final boolean thrownOut;
	private final int dieRoll;
	private final int defenseRating;
	private final int speedRating;

	public DefensiveThrowResult(DefensiveThrow defensiveThrow, boolean thrownOut, int dieRoll, int defenseRating,
			int speedRating) {
		this.defensiveThrow = defensiveThrow;
		this.thrownOut = thrownOut;
		this.dieRoll = dieRoll;
		this.defenseRating = defenseRating;
		this.speedRating = speedRating;
	}

	public DefensiveThrow getDefensiveThrow() {
		return defensiveThrow;
	}

	public boolean isThrownOut() {
		return thrownOut;
	}

	public int getDieRoll() {
		return dieRoll;
	}

	public int getDefenseRating() {
		return defenseRating;
	}

	public int getSpeedRating() {
		return speedRating;
	}
}

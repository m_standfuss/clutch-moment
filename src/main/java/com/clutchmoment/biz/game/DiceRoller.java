package com.clutchmoment.biz.game;

import java.util.Random;

public class DiceRoller {

	private final Random diceRandom;

	public DiceRoller() {
		diceRandom = new Random();
	}

	public int getRegularDiceRoll() {
		return diceRandom.nextInt(20) + 1;
	}

	public int getPowerDiceRoll() {
		return diceRandom.nextInt(24) + 1;
	}
}

package com.clutchmoment.biz.game;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.card.Pitcher;
import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.card.Stadium;
import com.clutchmoment.biz.card.Strategy;
import com.clutchmoment.biz.enums.Base;
import com.clutchmoment.biz.enums.Handedness;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.game.effect.BatterOnBaseClutchEffect;
import com.clutchmoment.biz.game.effect.BatterOnBaseEffect;
import com.clutchmoment.biz.game.effect.BatterRangeEffect;
import com.clutchmoment.biz.game.effect.BatterSwingClutchEffect;
import com.clutchmoment.biz.game.effect.BatterSwingEffect;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.MistakePitchEffect;
import com.clutchmoment.biz.game.effect.MultiEffect;
import com.clutchmoment.biz.game.effect.PitcherCommandClutchEffect;
import com.clutchmoment.biz.game.effect.PitcherCommandEffect;
import com.clutchmoment.biz.game.effect.PitcherInningsEffect;
import com.clutchmoment.biz.game.effect.PitcherPitchClutchEffect;
import com.clutchmoment.biz.game.effect.PitcherPitchEffect;
import com.clutchmoment.biz.game.effect.PitcherRangeEffect;
import com.clutchmoment.biz.game.player.GameCompetitor;
import com.clutchmoment.biz.game.player.Lineup;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class Game {

	private final GameCompetitor homeTeam;
	private final GameCompetitor awayTeam;

	private final Scorecard scorecard;
	private final DiceRoller diceRoller;

	private final List<Effect> gameEffects;
	private final List<Effect> atBatEffects;
	private final Multimap<Player, Effect> homePlayerEffects;
	private final Multimap<Player, Effect> awayPlayerEffects;

	private Integer lastPitchRoll;
	private AdvantageResult lastAdvantageResult;
	private Integer lastSwingRoll;
	private SwingResult lastSwingResult;
	private AdvanceRunnersRequest lastAdvanceRunnerRequest;
	private DefensiveThrowResult lastDefensiveThrowResult;

	public Game(GameCompetitor homeTeam, GameCompetitor awayTeam) {
		this(homeTeam, awayTeam, new DiceRoller());
	}

	public Game(GameCompetitor homeTeam, GameCompetitor awayTeam, DiceRoller diceRoller) {
		checkArgument(awayTeam != null && homeTeam != null, "Both competitors must be non-null.");
		checkArgument(diceRoller != null, "The dice roller cannot be null.");

		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.diceRoller = diceRoller;

		scorecard = new Scorecard();

		gameEffects = new ArrayList<>();
		atBatEffects = new ArrayList<>();
		homePlayerEffects = ArrayListMultimap.create();
		awayPlayerEffects = ArrayListMultimap.create();

		lastPitchRoll = null;
		lastAdvantageResult = null;
		lastSwingRoll = null;
		lastSwingResult = null;
		lastAdvanceRunnerRequest = null;
		lastDefensiveThrowResult = null;
	}

	public Scorecard getScorecard() {
		return scorecard;
	}

	public void startGame(Lineup homeLineup, Lineup awayLineup) {
		checkArgument(homeLineup.isFullLineup(), "The home lineup provided is not a full lineup.");
		checkArgument(awayLineup.isFullLineup(), "The away lineup provided is not a full lineup.");
		scorecard.startGame(homeLineup, awayLineup);
		Stadium stadium = homeTeam.getPlayerDeck().getStadium();
		try {
			gameEffects.add(makeEffect(stadium.getEffectClass()));
		} catch (Exception e) {
			throw new IllegalStateException(
					"Unable to load the effect class associated to the stadium for the home team.", e);
		}
	}

	public void makeSubstitution(GameCompetitor competitor, Player toSub, Player toSubFor) {
		checkArgument(competitor != null, "The competitor cannot be null.");
		checkArgument(toSub != null, "The player to substitute in cannot be null.");
		checkArgument(toSubFor != null, "The player to subsitute for cannot be null.");
		checkArgument(competitor.equals(homeTeam) || competitor.equals(awayTeam),
				"The competitor supplied does not match either of the competitors for this game.");
		if (competitor.equals(homeTeam)) {
			scorecard.makeHomeSubsitiution(toSub, toSubFor);
		} else {
			scorecard.makeAwaySubstitution(toSub, toSubFor);
		}
	}

	public Strategy drawCard(GameCompetitor gameCompetitor) {
		// TODO implement
		return null;
	}

	public void playPreInningStrategy() {

	}

	public void playSubstitionStrategy() {

	}

	public void playPrePitchStrategy() {

	}

	public int initiatePitch() {
		checkState(
				lastPitchRoll == null && lastAdvantageResult == null && lastSwingRoll == null
						&& lastSwingResult == null,
				"The previous play is not completed, follow the logical steps to complete each play.");
		int pitchDieRoll = diceRoller.getRegularDiceRoll();
		return lastPitchRoll = modifyPitchRoll(pitchDieRoll);
	}

	public AdvantageResult calculateAdvantage() {
		checkState(lastPitchRoll != null,
				"The last pitch roll was not set, call initiatePitch prior to calculating the advantage.");
		checkState(lastAdvantageResult == null, "The current advantage has already been calculated.");
		int pitcherCommand = modifyPitcherCommand(scorecard.getCurrentPitcher().getCommand());
		int batterOnBase = modifyBatterOnBase(scorecard.getCurrentBatter().getOnBase());
		boolean pitchersAdvantage = lastPitchRoll + pitcherCommand > batterOnBase;
		boolean mistakePitch = lastPitchRoll <= getPitcherMistakeMax();
		return lastAdvantageResult = new AdvantageResult(pitchersAdvantage, mistakePitch, lastPitchRoll, pitcherCommand, batterOnBase);
	}

	public void playAdvantageStrategy() {
		// TODO Implement
	}

	public int initiateSwing() {
		checkState(lastAdvantageResult != null,
				"The last advantage result must be calculated prior to rolling for the swing.");
		checkState(lastSwingRoll == null, "The current swing has already been calculated.");
		int dieRoll;
		if (lastAdvantageResult.isMistakePitch() || getInningsBeyondCommand() > 0) {
			dieRoll = diceRoller.getPowerDiceRoll();
		} else {
			dieRoll = diceRoller.getRegularDiceRoll();
		}
		return lastSwingRoll = modifyBatterSwingRoll(dieRoll);
	}

	public void playSwingStrategy(Strategy strategy) {
		// TODO Implement
	}

	public SwingResult finalizeSwingResult() {
		checkState(lastSwingRoll != null,
				"The last swing roll is not set, call initiateSwing prior to finalizing the result.");
		checkState(lastSwingResult == null, "The current swing result has already been decided.");
		PlayerRange playerRange;
		if (lastAdvantageResult.isPitcherAdvantage()) {
			playerRange = getPitcherPlayerRange();
		} else {
			playerRange = getBatterPlayerRange();
		}
		RangeOutcome outcome = playerRange.getRangeOutcome(lastSwingRoll);
		return lastSwingResult = new SwingResult(scorecard.getCurrentBatter(), scorecard.getCurrentPitcher(),
				lastSwingRoll, outcome);
	}

	public void playOutcomeStrategy(Strategy strategy) {

	}

	public void advanceRunners(AdvanceRunnersRequest request) {
		checkArgument(request != null, "The advance runners request cannot be null.");
		checkState(lastSwingResult != null,
				"The last swing result has not been calculated, unable to throw out runners ");
		checkState(
				lastSwingResult.getOutcome() == RangeOutcome.GROUNDBALL
						|| lastSwingResult.getOutcome() == RangeOutcome.FLYBALL
						|| lastSwingResult.getOutcome() == RangeOutcome.SINGLE
						|| lastSwingResult.getOutcome() == RangeOutcome.DOUBLE,
				"Runners can only be advanced on a groundball, flyball or a hit (except triple and homerun) ");
		checkState(validateAdvanceRunnersRequest(request), "The request to advance runners was invalid.");
		lastAdvanceRunnerRequest = request;
	}

	public void playPreDefenseStrategy() {
		// TODO Implement
	}

	public DefensiveThrowResult throwOutRunner(DefensiveThrow defensiveThrow) {
		checkArgument(defensiveThrow != null, "The defensive throw cannot be null.");
		checkState(lastAdvanceRunnerRequest != null, "There is no current runner advance request.");
		checkState(validateDefensiveThrow(defensiveThrow), "Invalid defense attempt.");
		Map<Base, Batter> runnerPositionAfterAutoAdvance = getRunnerPositionsAfterAutoAdvancement();
		Batter runner;
		if (defensiveThrow.isThrowHome()) {
			runner = runnerPositionAfterAutoAdvance.get(Base.THIRD);
		} else if (defensiveThrow.isThrowToThird()) {
			runner = runnerPositionAfterAutoAdvance.get(Base.SECOND);
		} else {// Throw is to second
			runner = runnerPositionAfterAutoAdvance.get(Base.FIRST);
		}

		int speed = runner.getSpeed();
		int defensiveRating;
		if (lastSwingResult.getOutcome() == RangeOutcome.GROUNDBALL) {
			defensiveRating = scorecard.getFieldingLineup().getInfieldDefensiveRating();
		} else if (lastSwingResult.getOutcome() == RangeOutcome.FLYBALL) {
			defensiveRating = scorecard.getFieldingLineup().getOutfieldDefensiveRating();
			defensiveRating += defensiveThrow.isThrowToSecond() ? 5 : 0;
		} else {
			// (+5) to the baserunnerís Speed if there were (2) outs before the Swing was
			// rolled.
			speed += scorecard.getGamePosition().getOuts() == 2 ? 5 : 0;
			// (+5) to the baserunnerís Speed if he is trying for Home.
			speed += defensiveThrow.isThrowHome() ? 5 : 0;
			defensiveRating = scorecard.getFieldingLineup().getOutfieldDefensiveRating();
		}
		int dieRoll = diceRoller.getRegularDiceRoll();
		boolean runnerThrownOut = defensiveRating > speed + dieRoll;
		return lastDefensiveThrowResult = new DefensiveThrowResult(defensiveThrow, runnerThrownOut, dieRoll,
				defensiveRating, speed);
	}

	public void playPostDefenseStrategy(Strategy strategyCard) {
	}

	public void playPostStrategyStrategy() {
		// TODO Implement
	}

	public void finalizeAtBat() {
		checkState(lastSwingResult != null,
				"The last swing was not initiated, call initiateSwing prior to finalizing the outcome.");
		scorecard.advanceGamePosition(lastSwingResult.getOutcome());
		if (lastAdvanceRunnerRequest != null) {
			/*
			 * Advance the runners starting at third and working backwards to avoid runner
			 * collision.
			 */

			if (lastAdvanceRunnerRequest.isAdvanceRunnerOnThird()) {
				if (lastDefensiveThrowResult.isThrownOut()
						&& lastDefensiveThrowResult.getDefensiveThrow().isThrowHome()) {
					scorecard.outAdvancingBases(Base.THIRD);
				} else {
					scorecard.advanceBase(Base.THIRD);
				}
			}

			if (lastAdvanceRunnerRequest.isAdvanceRunnerOnSecond()) {
				if (lastDefensiveThrowResult.isThrownOut()
						&& lastDefensiveThrowResult.getDefensiveThrow().isThrowToThird()) {
					scorecard.outAdvancingBases(Base.SECOND);
				} else {
					scorecard.advanceBase(Base.SECOND);
				}
			}

			if (lastAdvanceRunnerRequest.isAdvanceRunnerOnFirst()) {
				if (lastDefensiveThrowResult.isThrownOut()
						&& lastDefensiveThrowResult.getDefensiveThrow().isThrowToSecond()) {
					scorecard.outAdvancingBases(Base.FIRST);
				} else {
					scorecard.advanceBase(Base.FIRST);
				}
			}
		}
		/*
		 * Reset the current state to get ready for the next at bat sequence.
		 */
		lastPitchRoll = null;
		lastAdvantageResult = null;
		lastSwingRoll = null;
		lastSwingResult = null;
		lastAdvanceRunnerRequest = null;
		lastDefensiveThrowResult = null;
		atBatEffects.clear();
	}

	private int modifyPitcherCommand(int currentCommand) {
		int inningsBeyondCommand = getInningsBeyondCommand();
		currentCommand -= inningsBeyondCommand;
		currentCommand -= scorecard.getRunsScoredAgainstCurrentPitcher() % 4;
		currentCommand -= scorecard.getWalksAgainstCurrentPitcher() % 4;

		for (Effect effect : getCurrentEffects()) {
			if (isEffectActive(effect, PitcherCommandEffect.class)) {
				currentCommand = ((PitcherCommandEffect) effect).modifyPitcherCommand(currentCommand);
			}
		}

		if (isActiveEffect(PitcherCommandClutchEffect.class)) {
			currentCommand += scorecard.getCurrentPitcher().getClutch();
		}
		return currentCommand;
	}

	private int getInningsBeyondCommand() {
		int nextOut = scorecard.getOutsRecordedAgainstCurrentPitcher() + 1;
		int inningsLimit = scorecard.getCurrentPitcher().getInningsLimit();
		List<Effect> pitcherEffects = getCurrentPitcherEffects();
		for (Effect effect : pitcherEffects) {
			if (effect instanceof PitcherInningsEffect) {
				inningsLimit = ((PitcherInningsEffect) effect).modifyPitchersMaxInning(inningsLimit);
			}
		}
		int outsWithCommand = inningsLimit * 3;

		int outsBeyondCommand = nextOut - outsWithCommand;
		int inningsBeyondCommand = Math.max((int) Math.ceil(outsBeyondCommand / 3.0d), 0);
		return inningsBeyondCommand;
	}

	private int modifyPitchRoll(int currentPitchRoll) {
		Handedness pitcherHandness = scorecard.getCurrentPitcher().getMatchupHandedness();
		Handedness batterMatchupHandedness = scorecard.getCurrentBatter().getHandedness();
		if (pitcherHandness == batterMatchupHandedness) {
			currentPitchRoll += scorecard.getCurrentPitcher().getMatchupQualifier();
		}

		for (Effect effect : getCurrentEffects()) {
			if (isEffectActive(effect, PitcherPitchEffect.class)) {
				currentPitchRoll = ((PitcherPitchEffect) effect).modifyPitchersPitchRoll(currentPitchRoll);
			}
		}

		if (isActiveEffect(PitcherPitchClutchEffect.class)) {
			currentPitchRoll += scorecard.getCurrentPitcher().getClutch();
		}
		return currentPitchRoll;
	}

	private int modifyBatterOnBase(int currentOnBase) {
		Handedness pitcherHandness = scorecard.getCurrentPitcher().getHandedness();
		Handedness batterMatchupHandedness = scorecard.getCurrentBatter().getMatchupHandedness();
		if (pitcherHandness == batterMatchupHandedness) {
			currentOnBase += scorecard.getCurrentBatter().getMatchupQualifier();
		}

		for (Effect effect : getCurrentEffects()) {
			if (isEffectActive(effect, BatterOnBaseEffect.class)) {
				currentOnBase = ((BatterOnBaseEffect) effect).modifyBatterOnBase(currentOnBase);
			}
		}
		if (isActiveEffect(BatterOnBaseClutchEffect.class)) {
			currentOnBase += scorecard.getCurrentBatter().getClutch();
		}
		return currentOnBase;
	}

	private int modifyBatterSwingRoll(int currentDieRoll) {
		if (isActiveEffect(BatterSwingClutchEffect.class)) {
			currentDieRoll += scorecard.getCurrentBatter().getClutch();
		}
		for (Effect effect : getCurrentEffects()) {
			if (isEffectActive(effect, BatterSwingEffect.class)) {
				currentDieRoll = ((BatterSwingEffect) effect).modifyBatterSwingRollEffect(currentDieRoll);
			}
		}
		return currentDieRoll;
	}

	private int getPitcherMistakeMax() {
		int currentMax = scorecard.getCurrentPitcher().getMistakePitchMax();
		for (Effect effect : getCurrentEffects()) {
			if (isEffectActive(effect, MistakePitchEffect.class)) {
				currentMax = ((MistakePitchEffect) effect).modifyMistakePitchMax(currentMax);
			}
		}
		return currentMax;
	}

	private PlayerRange getBatterPlayerRange() {
		PlayerRange playerRange = new PlayerRange(scorecard.getCurrentBatter());
		for (Effect effect : getCurrentEffects()) {
			if (isEffectActive(effect, BatterRangeEffect.class)) {
				playerRange = ((BatterRangeEffect) effect).modifyBatterRange(playerRange, scorecard.getCurrentBatter());
			}
		}
		return playerRange;
	}

	private PlayerRange getPitcherPlayerRange() {
		PlayerRange playerRange = new PlayerRange(scorecard.getCurrentBatter());
		for (Effect effect : getCurrentEffects()) {
			if (isEffectActive(effect, PitcherRangeEffect.class)) {
				playerRange = ((PitcherRangeEffect) effect).modifyPitcherRange(playerRange);
			}
		}
		return playerRange;
	}

	private boolean isActiveEffect(Class<?> effectType) {
		for (Effect effect : getCurrentEffects()) {
			if (isEffectActive(effect, effectType)) {
				return true;
			}
		}
		return false;
	}

	private List<Effect> getCurrentPitcherEffects() {
		List<Effect> currentEffects = getCurrentEffects();
		Pitcher pitcher = scorecard.getCurrentPitcher();
		if (scorecard.getGamePosition().isTopOfInning()) {
			currentEffects.addAll(homePlayerEffects.get(pitcher));
		} else {
			currentEffects.addAll(awayPlayerEffects.get(pitcher));
		}
		return currentEffects;
	}

	private List<Effect> getCurrentEffects() {
		List<Effect> activeEffects = new ArrayList<>(gameEffects);
		activeEffects.addAll(atBatEffects);
		return activeEffects;
	}

	private boolean isEffectActive(Effect effect, Class<?> effectType) {
		return isEffectActive(effect, effectType, getCurrentGameSituation());
	}

	private boolean isEffectActive(Effect effect, Class<?> effectType, GameSituation situation) {
		if (!effectType.isInstance(effect)) {
			return false;
		}
		if (effect instanceof MultiEffect) {
			MultiEffect multiEffect = (MultiEffect) effect;
			if (multiEffect.getSecondEffectType() == effectType) {
				return multiEffect.isSecondEffectActive(situation);
			}
		}
		return effect.isActiveEffect(situation);
	}

	private Effect makeEffect(String effectClass) throws Exception {
		Class<?> clazz = Class.forName(effectClass);
		Constructor<?> ctor = clazz.getConstructor();
		Object object = ctor.newInstance();
		return (Effect) object;
	}

	private boolean validateAdvanceRunnersRequest(AdvanceRunnersRequest request) {
		Map<Base, Batter> baseToBatterAdvancedTo = getRunnerPositionsAfterAutoAdvancement();

		Batter runnerOnFirstAfterSwing = baseToBatterAdvancedTo.get(Base.FIRST);
		Batter runnerOnSecondAfterSwing = baseToBatterAdvancedTo.get(Base.SECOND);
		Batter runnerOnThirdAfterSwing = baseToBatterAdvancedTo.get(Base.THIRD);
		if (request.isAdvanceRunnerOnFirst()) {
			if (runnerOnFirstAfterSwing == null || runnerOnFirstAfterSwing.equals(scorecard.getCurrentBatter())) {
				return false;
			}
			if (runnerOnSecondAfterSwing != null && !request.isAdvanceRunnerOnSecond()) {
				return false;
			}
		}
		if (request.isAdvanceRunnerOnSecond()) {
			if (runnerOnSecondAfterSwing == null || runnerOnSecondAfterSwing.equals(scorecard.getCurrentBatter())) {
				return false;
			}
			if (runnerOnThirdAfterSwing != null && !request.isAdvanceRunnerOnThird()) {
				return false;
			}
		}
		if (request.isAdvanceRunnerOnThird()) {
			if (runnerOnThirdAfterSwing == null || runnerOnThirdAfterSwing.equals(scorecard.getCurrentBatter())) {
				return false;
			}
		}
		return true;
	}

	private boolean validateDefensiveThrow(DefensiveThrow defensiveThrow) {
		if (defensiveThrow.isThrowHome() && !lastAdvanceRunnerRequest.isAdvanceRunnerOnThird()) {
			return false;
		}
		if (defensiveThrow.isThrowToThird() && !lastAdvanceRunnerRequest.isAdvanceRunnerOnSecond()) {
			return false;
		}
		if (defensiveThrow.isThrowToSecond() && !lastAdvanceRunnerRequest.isAdvanceRunnerOnFirst()) {
			return false;
		}
		return true;
	}

	private Map<Base, Batter> getRunnerPositionsAfterAutoAdvancement() {
		Map<Base, Batter> runnerPositions = new HashMap<>();
		switch (lastSwingResult.getOutcome()) {
		case SINGLE:
		case WALK:
			runnerPositions.put(Base.FIRST, scorecard.getCurrentBatter());
			runnerPositions.put(Base.SECOND, scorecard.getRunnerOnFirst());
			runnerPositions.put(Base.THIRD, scorecard.getRunnerOnSecond());
			runnerPositions.put(Base.HOME, scorecard.getRunnerOnThird());
			break;
		case DOUBLE:
			runnerPositions.put(Base.FIRST, null);
			runnerPositions.put(Base.SECOND, scorecard.getCurrentBatter());
			runnerPositions.put(Base.THIRD, scorecard.getRunnerOnFirst());
			runnerPositions.put(Base.HOME, scorecard.getRunnerOnSecond());
		case TRIPLE:
			runnerPositions.put(Base.FIRST, null);
			runnerPositions.put(Base.SECOND, null);
			runnerPositions.put(Base.THIRD, scorecard.getCurrentBatter());
			runnerPositions.put(Base.HOME, scorecard.getRunnerOnFirst());
		case HOMERUN:
			runnerPositions.put(Base.FIRST, null);
			runnerPositions.put(Base.SECOND, null);
			runnerPositions.put(Base.THIRD, null);
			runnerPositions.put(Base.HOME, scorecard.getCurrentBatter());
		case GROUNDBALL:
			runnerPositions.put(Base.FIRST, null);
			runnerPositions.put(Base.SECOND, scorecard.getRunnerOnFirst());
			runnerPositions.put(Base.THIRD, scorecard.getRunnerOnSecond());
		default:
			runnerPositions.put(Base.FIRST, scorecard.getRunnerOnFirst());
			runnerPositions.put(Base.SECOND, scorecard.getRunnerOnSecond());
			runnerPositions.put(Base.THIRD, scorecard.getRunnerOnThird());
		}
		return runnerPositions;
	}

	private GameSituation getCurrentGameSituationForPlayerAdvancing(Player playerAttemptingToAdvance) {
		GameSituation situation = getCurrentGameSituation();
		situation.setPlayerAttemptingToAdvance(playerAttemptingToAdvance);
		return situation;
	}

	private GameSituation getCurrentGameSituation() {
		GameSituation situation = new GameSituation();
		situation.setScorecard(scorecard);
		situation.setLastAdvantageResult(lastAdvantageResult);
		situation.setLastSwingResult(lastSwingResult);
		return situation;
	}
}

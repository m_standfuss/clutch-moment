package com.clutchmoment.biz.game;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.util.*;

import com.clutchmoment.biz.game.player.GameCompetitor;
import com.clutchmoment.biz.game.player.Lineup;

public class GameBuilder {

	private GameCompetitor competitor1;
	private GameCompetitor competitor2;

	private GameCompetitor homeTeam;
	private GameCompetitor awayTeam;

	private final Map<GameCompetitor, Lineup> competitorToLineup;

	public GameBuilder() {
		competitorToLineup = new HashMap<>();
	}

	public void addCompetitor(GameCompetitor competitor) {
		checkState(competitor1 == null || competitor2 == null,
				"Two competitors have already been set for the game builder.");
		if (competitor1 == null) {
			competitor1 = competitor;
		} else if (competitor2 == null) {
			competitor2 = competitor;
		}
	}

	public void setHomeTeam(GameCompetitor competitor) {
		checkState(isCompetitor(competitor), "The competitor supplied doesnt match a current competitor of the game.");
		homeTeam = competitor;
	}

	public void setAwayTeam(GameCompetitor competitor) {
		checkState(isCompetitor(competitor), "The competitor supplied doesnt match a current competitor of the game.");
		awayTeam = competitor;
	}

	public void setLineup(GameCompetitor competitor, Lineup lineup) {
		checkArgument(competitor != null, "The competitor supplied cannot be null.");
		checkArgument(lineup != null, "The lineup supplied cannot be null.");
		checkArgument(lineup.isFullLineup(), "The lineup supplied is not a full lineup.");
		checkState(isCompetitor(competitor), "The competitor supplied doesnt match a current competitor of the game.");
		competitorToLineup.put(competitor, lineup);
	}

	public Game startGame() {
		checkState(competitor1 != null && competitor2 != null,
				"Both competitors must be set before starting the game.");
		checkState(competitorToLineup.containsKey(competitor1) && competitorToLineup.containsKey(competitor2), "Each competitor must have a valid lineup before starting the game.");

		GameCompetitor localHomeTeam;
		GameCompetitor localAwayTeam;
		if (homeTeam == null && awayTeam == null) {
			localHomeTeam = getRandomCompetitor();
			localAwayTeam = getOtherCompetitor(localHomeTeam);
		} else {
			if (homeTeam != null) {
				localHomeTeam = homeTeam;
				localAwayTeam = getOtherCompetitor(localHomeTeam);
			} else {
				localAwayTeam = awayTeam;
				localHomeTeam = getOtherCompetitor(localAwayTeam);
			}
		}
		Game game = new Game(localHomeTeam, localAwayTeam);
		game.startGame(competitorToLineup.get(localHomeTeam), competitorToLineup.get(localAwayTeam));
		return game;
	}

	private GameCompetitor getRandomCompetitor() {
		List<GameCompetitor> competitors = Arrays.asList(competitor1, competitor2);
		Random rand = new Random();
		return competitors.get(rand.nextInt(competitors.size()));
	}

	private GameCompetitor getOtherCompetitor(GameCompetitor competitor) {
		if (Objects.equals(competitor, competitor1)) {
			return competitor2;
		}
		return competitor1;
	}

	private boolean isCompetitor(GameCompetitor competitor) {
		return Objects.equals(competitor, competitor1) || Objects.equals(competitor, competitor2);
	}

}

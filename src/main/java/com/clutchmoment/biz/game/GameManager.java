package com.clutchmoment.biz.game;

import static com.google.common.base.Preconditions.checkState;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.clutchmoment.biz.game.player.GameCompetitor;
import com.clutchmoment.biz.game.player.Lineup;

public class GameManager {

	private final Map<UUID, GameBuilder> uuidToBuilder;
	private final Map<UUID, Game> uuidToGame;

	public GameManager() {
		uuidToBuilder = new HashMap<>();
		uuidToGame = new HashMap<>();
	}

	public UUID createGame() {
		UUID gameUUID = UUID.randomUUID();
		GameBuilder builder = new GameBuilder();
		uuidToBuilder.put(gameUUID, builder);
		return gameUUID;
	}

	public void joinGame(UUID gameUUID, GameCompetitor competitor) {
		checkState(uuidToBuilder.containsKey(gameUUID), "The game to join for the provided key was not found.");
		uuidToBuilder.get(gameUUID).addCompetitor(competitor);
	}

	public void setLineup(UUID gameUUID, GameCompetitor competitor, Lineup lineup){
		checkState(uuidToBuilder.containsKey(gameUUID), "The game to join for the provided key was not found.");
		uuidToBuilder.get(gameUUID).setLineup(competitor, lineup);
	}

	public void startGame(UUID gameUUID) {
		checkState(uuidToBuilder.containsKey(gameUUID), "The game to join for the provided key was not found.");
		GameBuilder builder = uuidToBuilder.get(gameUUID);
		Game game = builder.startGame();
		uuidToGame.put(gameUUID, game);
		uuidToBuilder.remove(gameUUID);
	}

	public Game getGame(UUID gameUUID) {
		checkState(uuidToGame.containsKey(gameUUID), "The game for the provided key was not found.");
		return uuidToGame.get(gameUUID);
	}
}

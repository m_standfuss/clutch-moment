package com.clutchmoment.biz.game;

public class GamePosition {

	private int inning;
	private boolean topInning;
	private int outs;

	private int awayHittingPosition;
	private int homeHittingPosition;

	public GamePosition() {
		inning = 1;
		topInning = true;
		outs = 0;
		awayHittingPosition = 1;
		homeHittingPosition = 1;
	}

	public boolean isTopOfInning() {
		return topInning;
	}

	public int getInning() {
		return inning;
	}

	public int getOuts() {
		return outs;
	}

	public int getAwayHittingPosition() {
		return awayHittingPosition;
	}

	public int getHomeHittingPosition() {
		return homeHittingPosition;
	}

	void recordOut() {
		incrementHitterPosition();
		if (outs == 2) {
			incrementInning();
		} else {
			outs++;
		}
	}

	void nextHitter() {
		incrementHitterPosition();
	}

	private void incrementInning() {
		outs = 0;
		if (!topInning) {
			inning++;
		}
		topInning = !topInning;
	}

	private void incrementHitterPosition() {
		if (topInning) {
			awayHittingPosition = getNextHitterPosition(awayHittingPosition);
		} else {
			homeHittingPosition = getNextHitterPosition(homeHittingPosition);
		}
	}

	private int getNextHitterPosition(int currentPosition) {
		if (currentPosition == 9) {
			return 1;
		}
		return ++currentPosition;
	}
}

package com.clutchmoment.biz.game;

import com.clutchmoment.biz.card.Player;

public class GameSituation {

	private Scorecard scorecard;

	private Player playerAttemptingToAdvance;

	private int pitchDieRoll;
	private int swingDieRoll;

	private AdvantageResult lastAdvantageResult;
	private SwingResult lastSwingResult;

	public Scorecard getScorecard() {
		return scorecard;
	}

	public void setScorecard(Scorecard scorecard) {
		this.scorecard = scorecard;
	}

	public Player getPlayerAttemptingToAdvance() {
		return playerAttemptingToAdvance;
	}

	public void setPlayerAttemptingToAdvance(Player playerAttemptingToAdvance) {
		this.playerAttemptingToAdvance = playerAttemptingToAdvance;
	}

	public int getPitchDieRoll() {
		return pitchDieRoll;
	}

	public void setPitchDieRoll(int pitchDieRoll) {
		this.pitchDieRoll = pitchDieRoll;
	}

	public int getSwingDieRoll() {
		return swingDieRoll;
	}

	public void setSwingDieRoll(int swingDieRoll) {
		this.swingDieRoll = swingDieRoll;
	}

	public AdvantageResult getLastAdvantageResult() {
		return lastAdvantageResult;
	}

	public void setLastAdvantageResult(AdvantageResult lastAdvantageResult) {
		this.lastAdvantageResult = lastAdvantageResult;
	}

	public SwingResult getLastSwingResult() {
		return lastSwingResult;
	}

	public void setLastSwingResult(SwingResult lastSwingResult) {
		this.lastSwingResult = lastSwingResult;
	}
}

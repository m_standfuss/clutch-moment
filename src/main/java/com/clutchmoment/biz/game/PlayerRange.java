package com.clutchmoment.biz.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.javatuples.Pair;

import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.google.common.collect.Range;

public class PlayerRange {

	private final List<Pair<Range<Integer>, RangeOutcome>> rangeAndOutcomes;
	private final Map<RangeOutcome, RangeOutcome> transformationMap;

	public PlayerRange(Player player) {
		rangeAndOutcomes = makeRangeOutcomes(player);
		transformationMap = new HashMap<>();
	}

	public PlayerRange(PlayerRange toCopy, RangeOutcome transformOutcome, RangeOutcome toOutcome) {
		this(toCopy);
		transformationMap.put(transformOutcome, toOutcome);
	}

	public PlayerRange(PlayerRange toCopy, boolean highEnd, RangeOutcome transformOutcome, RangeOutcome toOutcome) {
		this(toCopy);
		Range<Integer> currentRange = getRangeForOutcome(transformOutcome);
		Range<Integer> newRange;
		int missingNumber;
		if (highEnd) {
			newRange = Range.closed(currentRange.lowerEndpoint(), currentRange.upperEndpoint() - 1);
			missingNumber = currentRange.upperEndpoint();
		} else {
			newRange = Range.closed(currentRange.lowerEndpoint() + 1, currentRange.upperEndpoint());
			missingNumber = currentRange.lowerEndpoint();
		}
		replaceRangeForOutcome(transformOutcome, newRange);
		rangeAndOutcomes.add(Pair.with(Range.closed(missingNumber, missingNumber), toOutcome));
	}

	public PlayerRange(PlayerRange toCopy, RangeOutcome transformOutcome, int deltaChange) {
		this(toCopy);
		Range<Integer> currentRange = getRangeForOutcome(transformOutcome);
		if (currentRange == null) {
			/*
			 * If the outcome to change is currently not mapped to a range then nothing to
			 * do.
			 */
			return;
		}
		if (deltaChange > 0) {
			/*
			 * We are increasing the max on this outcome, so the next range's minimum needs
			 * to be increased to follow suit.
			 */
			Range<Integer> newRange = Range.closed(currentRange.lowerEndpoint(),
					currentRange.upperEndpoint() + deltaChange);
			replaceRangeForOutcome(transformOutcome, newRange);
			RangeOutcome outcomeToPullFrom = getNextOutcome(transformOutcome);
			while (outcomeToPullFrom != null && getRangeForOutcome(outcomeToPullFrom) != null) {
				outcomeToPullFrom = getNextOutcome(outcomeToPullFrom);
			}
			if (outcomeToPullFrom != null) {
				Range<Integer> currentRangeToPullFrom = getRangeForOutcome(outcomeToPullFrom);
				Range<Integer> newRangeToPullFrom = Range.closed(currentRangeToPullFrom.lowerEndpoint() + deltaChange,
						currentRangeToPullFrom.upperEndpoint());
				replaceRangeForOutcome(outcomeToPullFrom, newRangeToPullFrom);
			}
		} else {
			/*
			 * We are decreasing the minimum on this outcome, so previous range's max also
			 * needs to be decreased.
			 */
			Range<Integer> newRange = Range.closed(currentRange.lowerEndpoint() + deltaChange,
					currentRange.upperEndpoint());
			replaceRangeForOutcome(transformOutcome, newRange);
			RangeOutcome outcomeToPullFrom = getPreviousOutcome(transformOutcome);
			while (outcomeToPullFrom != null && getRangeForOutcome(outcomeToPullFrom) != null) {
				outcomeToPullFrom = getPreviousOutcome(outcomeToPullFrom);
			}
			if (outcomeToPullFrom != null) {
				Range<Integer> currentRangeToPullFrom = getRangeForOutcome(outcomeToPullFrom);
				Range<Integer> newRangeToPullFrom = Range.closed(currentRangeToPullFrom.lowerEndpoint(),
						currentRangeToPullFrom.upperEndpoint() + deltaChange);
				replaceRangeForOutcome(outcomeToPullFrom, newRangeToPullFrom);
			}
		}
	}

	private PlayerRange(PlayerRange toCopy) {
		rangeAndOutcomes = new ArrayList<>(toCopy.rangeAndOutcomes);
		transformationMap = new HashMap<>(toCopy.transformationMap);
	}

	public RangeOutcome getRangeOutcome(int value) {
		RangeOutcome outcome = null;
		for (Pair<Range<Integer>, RangeOutcome> outcomePair : rangeAndOutcomes) {
			if (outcomePair.getValue0().contains(value)) {
				outcome = outcomePair.getValue1();
				break;
			}
		}
		if (transformationMap.containsKey(outcome)) {
			outcome = transformationMap.get(outcome);
		}
		return outcome;
	}

	private void replaceRangeForOutcome(RangeOutcome outcome, Range<Integer> newRange) {
		Integer indexToReplaceAt = null;
		for (int i = 0; i < rangeAndOutcomes.size(); i++) {
			if (rangeAndOutcomes.get(i).getValue1() == outcome) {
				indexToReplaceAt = i;
				break;
			}
		}
		if (indexToReplaceAt == null) {
			return;
		}
		rangeAndOutcomes.set(indexToReplaceAt, Pair.with(newRange, outcome));
	}

	private Range<Integer> getRangeForOutcome(RangeOutcome outcome) {
		for (Pair<Range<Integer>, RangeOutcome> outcomePair : rangeAndOutcomes) {
			if (outcomePair.getValue1() == outcome) {
				return outcomePair.getValue0();
			}
		}
		return null;
	}

	private static RangeOutcome getNextOutcome(RangeOutcome outcome) {
		for (int i = 0; i < RangeOutcome.values().length; i++) {
			RangeOutcome value = RangeOutcome.values()[i];
			if (value == outcome) {
				if (i + 1 < RangeOutcome.values().length) {
					return RangeOutcome.values()[i + 1];
				} else {
					break;
				}
			}
		}
		return null;
	}

	private static RangeOutcome getPreviousOutcome(RangeOutcome outcome) {
		for (int i = RangeOutcome.values().length - 1; i <= 0; i--) {
			RangeOutcome value = RangeOutcome.values()[i];
			if (value == outcome) {
				if (i - 1 <= 0) {
					return RangeOutcome.values()[i - 1];
				} else {
					break;
				}
			}
		}
		return null;
	}

	private static List<Pair<Range<Integer>, RangeOutcome>> makeRangeOutcomes(Player player) {
		List<Pair<Range<Integer>, RangeOutcome>> rangeAndOutcomes = new ArrayList<>();
		Map<RangeOutcome, Integer> outcomeToMaxMap = getRangeOutcomeToMaxMap(player);
		int currentLow = 0;
		for (RangeOutcome outcome : RangeOutcome.values()) {
			if (outcomeToMaxMap.get(outcome) == null) {
				continue;
			}

			int maxForRange = outcomeToMaxMap.get(outcome);
			rangeAndOutcomes.add(Pair.with(Range.closed(currentLow, maxForRange), outcome));
			currentLow = maxForRange + 1;
		}
		return rangeAndOutcomes;
	}

	private static Map<RangeOutcome, Integer> getRangeOutcomeToMaxMap(Player player) {
		Map<RangeOutcome, Integer> outcomeToMaxMap = new HashMap<>();
		outcomeToMaxMap.put(RangeOutcome.STRIKEOUT, player.getResultStrikeout());
		outcomeToMaxMap.put(RangeOutcome.GROUNDBALL, player.getResultGroundball());
		outcomeToMaxMap.put(RangeOutcome.FLYBALL, player.getResultFlyball());
		outcomeToMaxMap.put(RangeOutcome.WALK, player.getResultWalk());
		outcomeToMaxMap.put(RangeOutcome.SINGLE, player.getResultSingle());
		outcomeToMaxMap.put(RangeOutcome.DOUBLE, player.getResultDouble());
		outcomeToMaxMap.put(RangeOutcome.TRIPLE, player.getResultTriple());
		outcomeToMaxMap.put(RangeOutcome.HOMERUN, Integer.MAX_VALUE);
		return outcomeToMaxMap;
	}
}

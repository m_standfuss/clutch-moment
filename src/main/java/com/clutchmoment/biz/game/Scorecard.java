package com.clutchmoment.biz.game;

import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.card.Pitcher;
import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.enums.Base;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.game.player.Lineup;

public class Scorecard {

	private final GamePosition gamePosition;

	private final Map<Pitcher, Integer> homePitcherToOutsRecorded;
	private final Map<Pitcher, Integer> homePitcherToWalks;
	private final Map<Pitcher, Integer> homePitcherToRuns;

	private final Map<Pitcher, Integer> awayPitcherToOutsRecorded;
	private final Map<Pitcher, Integer> awayPitcherToWalks;
	private final Map<Pitcher, Integer> awayPitcherToRuns;

	private final Map<Integer, Integer> homeRunsScored;
	private final Map<Integer, Integer> awayRunsScored;

	private final Map<Player, Integer> homeHomerunCount;
	private final Map<Player, Integer> awayHomerunCount;

	private final List<RangeOutcome> outcomes;

	private boolean gameStarted;

	private Lineup homeLineup;
	private Lineup awayLineup;

	private Batter runnerOnFirst;
	private Batter runnerOnSecond;
	private Batter runnerOnThird;

	private boolean gameOver;

	Scorecard() {
		gamePosition = new GamePosition();
		homePitcherToOutsRecorded = new HashMap<>();
		homePitcherToWalks = new HashMap<>();
		homePitcherToRuns = new HashMap<>();
		awayPitcherToOutsRecorded = new HashMap<>();
		awayPitcherToWalks = new HashMap<>();
		awayPitcherToRuns = new HashMap<>();
		homeRunsScored = new HashMap<>();
		awayRunsScored = new HashMap<>();
		homeHomerunCount = new HashMap<>();
		awayHomerunCount = new HashMap<>();
		outcomes = new ArrayList<>();
		gameStarted = false;
	}

	public boolean isGameStarted() {
		return gameStarted;
	}

	public void startGame(Lineup homeLineup, Lineup awayLineup) {
		this.homeLineup = homeLineup;
		this.awayLineup = awayLineup;

		runnerOnFirst = null;
		runnerOnSecond = null;
		runnerOnThird = null;
		gameStarted = true;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public GamePosition getGamePosition() {
		return gamePosition;
	}

	public Pitcher getCurrentPitcher() {
		return gamePosition.isTopOfInning() ? homeLineup.getPitcher() : awayLineup.getPitcher();
	}

	public Batter getCurrentBatter() {
		int battingPosition;
		Lineup lineup;
		if (gamePosition.isTopOfInning()) {
			battingPosition = gamePosition.getAwayHittingPosition();
			lineup = awayLineup;
		} else {
			battingPosition = gamePosition.getHomeHittingPosition();
			lineup = homeLineup;
		}
		return lineup.getBattingOrder().getBatter(battingPosition);
	}

	public Pitcher getOtherTeamsPitcher() {
		return gamePosition.isTopOfInning() ? awayLineup.getPitcher() : homeLineup.getPitcher();
	}

	public Map<Player, Integer> getPlayerToHomeRunCount(boolean homeTeam) {
		Map<Player, Integer> map = homeTeam ? homeHomerunCount : awayHomerunCount;
		return new HashMap<>(map);
	}

	public boolean isFieldingTeamLosing() {
		if (gamePosition.isTopOfInning()) {
			return getHomeRunsScored() < getAwayRunsScored();
		}
		return getAwayRunsScored() < getHomeRunsScored();
	}

	public boolean isBattingTeamLosing() {
		if (gamePosition.isTopOfInning()) {
			return getAwayRunsScored() < getHomeRunsScored();
		}
		return getHomeRunsScored() < getAwayRunsScored();
	}

	public Lineup getFieldingLineup() {
		Lineup fieldingLineup;
		if (gamePosition.isTopOfInning()) {
			fieldingLineup = homeLineup;
		} else {
			fieldingLineup = awayLineup;
		}
		return fieldingLineup;
	}

	public int getHomeRunsScored() {
		int runsScored = 0;
		for (int i = 1; i <= gamePosition.getInning(); i++) {
			runsScored += homeRunsScored.getOrDefault(i, 0);
		}
		return runsScored;
	}

	public int getAwayRunsScored() {
		int runsScored = 0;
		for (int i = 1; i <= gamePosition.getInning(); i++) {
			runsScored += awayRunsScored.getOrDefault(i, 0);
		}
		return runsScored;
	}

	public boolean isRunnerInScoringPosition() {
		return runnerOnSecond != null || runnerOnThird != null;
	}

	public Batter getRunnerOnFirst() {
		return runnerOnFirst;
	}

	public Batter getRunnerOnSecond() {
		return runnerOnSecond;
	}

	public Batter getRunnerOnThird() {
		return runnerOnThird;
	}

	public int getOutsRecordedAgainstCurrentPitcher() {
		return getOutsRecorded(gamePosition.isTopOfInning(), getCurrentPitcher());
	}

	public int getOutsRecorded(boolean homeTeam, Pitcher pitcher) {
		Map<Pitcher, Integer> pitcherOutMap = homeTeam ? homePitcherToOutsRecorded : awayPitcherToOutsRecorded;
		return pitcherOutMap.getOrDefault(pitcher, 0);
	}

	public int getWalksAgainstCurrentPitcher() {
		return getWalks(gamePosition.isTopOfInning(), getCurrentPitcher());
	}

	public int getWalks(boolean homeTeam, Pitcher pitcher) {
		Map<Pitcher, Integer> pitcherWalksMap = homeTeam ? homePitcherToWalks : awayPitcherToWalks;
		return pitcherWalksMap.getOrDefault(pitcher, 0);
	}

	public int getRunsScoredAgainstCurrentPitcher() {
		return getRunsScoredAgainst(gamePosition.isTopOfInning(), getCurrentPitcher());
	}

	public int getRunsScoredAgainst(boolean homeTeam, Pitcher pitcher) {
		Map<Pitcher, Integer> pitcherRunsMap = homeTeam ? homePitcherToRuns : awayPitcherToRuns;
		return pitcherRunsMap.getOrDefault(pitcher, 0);
	}

	public RangeOutcome getLastPlay() {
		List<RangeOutcome> lastOutcomes = getLastPlays(1);
		return lastOutcomes.isEmpty() ? null : lastOutcomes.get(0);
	}

	public List<RangeOutcome> getLastPlays(int numberOfAtBats) {
		List<RangeOutcome> outcomesToReturn = new ArrayList<>();
		int index = outcomes.size() - 1;
		while (outcomesToReturn.size() < numberOfAtBats) {
			if (index < 0) {
				break;
			}
			outcomesToReturn.add(outcomes.get(index));
			index--;
		}
		return outcomesToReturn;
	}

	void advanceGamePosition(RangeOutcome outcome) {
		outcomes.add(outcome);
		switch (outcome) {
		case STRIKEOUT:
			addStrikeOut();
			break;
		case GROUNDBALL:
			addGroundBallOut();
			break;
		case FLYBALL:
			addFlyBallOut();
			break;
		case SINGLE:
			addSingle();
			break;
		case DOUBLE:
			addDouble();
			break;
		case TRIPLE:
			addTriple();
			break;
		case HOMERUN:
			addHomerun();
			break;
		case WALK:
			addWalk();
			break;
		}
	}

	void addIntentionalWalk() {
		outcomes.add(RangeOutcome.WALK);
		addWalk(true);
	}

	void makeHomeSubsitiution(Player playerComingIn, Player playerLeaving) {
		homeLineup.makeSubstitution(playerComingIn, playerLeaving);
		if (!gamePosition.isTopOfInning()) {
			replaceRunners(playerComingIn, playerLeaving);
		}
	}

	void makeAwaySubstitution(Player playerComingIn, Player playerLeaving) {
		homeLineup.makeSubstitution(playerComingIn, playerLeaving);
		if (gamePosition.isTopOfInning()) {
			replaceRunners(playerComingIn, playerLeaving);
		}
	}

	private void addStrikeOut() {
		incrementOut();
	}

	private void addFlyBallOut() {
		incrementOut();
	}

	private void addGroundBallOut() {
		incrementOut();
	}

	private void addSingle() {
		incrementBaseRunners();
		runnerOnFirst = getCurrentBatter();
		gamePosition.nextHitter();
	}

	private void addDouble() {
		incrementBaseRunners();
		incrementBaseRunners();
		runnerOnSecond = getCurrentBatter();
		gamePosition.nextHitter();
	}

	private void addTriple() {
		incrementBaseRunners();
		incrementBaseRunners();
		incrementBaseRunners();
		runnerOnThird = getCurrentBatter();
		gamePosition.nextHitter();
	}

	private void addHomerun() {
		incrementBaseRunners();
		incrementBaseRunners();
		incrementBaseRunners();
		scoreRun();
		incrementCountOnMap(getCurrentBatter(), gamePosition.isTopOfInning() ? awayHomerunCount : homeHomerunCount);
		gamePosition.nextHitter();
	}

	private void addWalk() {
		addWalk(false);
	}

	void outAdvancingBases(Base base) {
		checkState(validateRunnerOnBase(base), "The base supplied as an out advancing bases doesnt contain a runner.");
		incrementOut();
		if (base == Base.FIRST) {
			runnerOnFirst = null;
		} else if (base == Base.SECOND) {
			runnerOnSecond = null;
		} else if (base == Base.THIRD) {
			runnerOnThird = null;
		}
	}

	/**
	 * This should be called in greatest base to lowest base order (third, then
	 * second then first) to avoid base runner collision.
	 * 
	 * @param base The base the runner is currently on to advance.
	 */
	void advanceBase(Base base) {
		checkState(validateRunnerOnBase(base), "The base supplied to advance the runner on does not contain a runner.");
		if (base == Base.FIRST) {
			runnerOnSecond = runnerOnFirst;
		} else if (base == Base.SECOND) {
			runnerOnThird = runnerOnSecond;
		} else if (base == Base.THIRD) {
			scoreRun();
			runnerOnThird = null;
		}
	}

	private void addWalk(boolean intentional) {
		incrementBaseRunners();
		runnerOnFirst = getCurrentBatter();
		gamePosition.nextHitter();
		if (!intentional) {
			incrementCountOnMap(getCurrentPitcher(),
					gamePosition.isTopOfInning() ? homePitcherToWalks : awayPitcherToWalks);
		}
	}

	private void incrementBaseRunners() {
		if (runnerOnThird != null) {
			scoreRun();
			runnerOnThird = null;
		}
		if (runnerOnSecond != null) {
			runnerOnThird = runnerOnSecond;
			runnerOnSecond = null;
		}
		if (runnerOnFirst != null) {
			runnerOnSecond = runnerOnFirst;
			runnerOnFirst = null;
		}
		runnerOnFirst = null;
	}

	private void scoreRun() {
		Map<Integer, Integer> runMap;
		if (gamePosition.isTopOfInning()) {
			runMap = awayRunsScored;
		} else {
			runMap = homeRunsScored;
		}
		incrementCountOnMap(gamePosition.getInning(), runMap);
		incrementCountOnMap(getCurrentPitcher(), gamePosition.isTopOfInning() ? homePitcherToRuns : awayPitcherToRuns);
		checkEndOfGameRunScored();
	}

	private void incrementOut() {
		checkEndOfGameBeforeOutRecorded();
		incrementCountOnMap(getCurrentPitcher(),
				gamePosition.isTopOfInning() ? homePitcherToOutsRecorded : awayPitcherToOutsRecorded);

		gamePosition.recordOut();
		if (gamePosition.getOuts() == 0) {
			/*
			 * New inning clear the bases
			 */
			runnerOnFirst = null;
			runnerOnSecond = null;
			runnerOnThird = null;
		}
	}

	private void checkEndOfGameRunScored() {
		if (gamePosition.getInning() < 9 || gamePosition.isTopOfInning()) {
			return;
		}
		/**
		 * Check if a walkoff just occured after scoring runs.
		 */
		gameOver = getHomeRunsScored() > getAwayRunsScored();
	}

	private void checkEndOfGameBeforeOutRecorded() {
		if (gamePosition.getInning() < 9 || gamePosition.getOuts() != 2) {
			return;
		}
		/*
		 * We are at the end of the home or away half in the 9th/10th/etc.
		 */
		if (gamePosition.isTopOfInning()) {
			/*
			 * If the home team is winning after the top half of the last inning we dont
			 * have to play the bottom half.
			 */
			gameOver = getHomeRunsScored() > getAwayRunsScored();
		} else {
			/*
			 * If the score is not tied after the bottom half of the last inning the game is
			 * over.
			 */
			gameOver = getHomeRunsScored() != getAwayRunsScored();
		}
	}

	private void replaceRunners(Player playerComingIn, Player playerLeaving) {
		if (!(playerComingIn instanceof Batter)) {
			return;
		}
		if (playerLeaving.equals(runnerOnFirst)) {
			runnerOnFirst = (Batter) playerComingIn;
		}
		if (playerLeaving.equals(runnerOnSecond)) {
			runnerOnSecond = (Batter) playerComingIn;
		}
		if (playerLeaving.equals(runnerOnThird)) {
			runnerOnThird = (Batter) playerComingIn;
		}
	}

	private boolean validateRunnerOnBase(Base base) {
		switch (base) {
		case FIRST:
			return runnerOnFirst != null;
		case SECOND:
			return runnerOnSecond != null;
		case THIRD:
			return runnerOnThird != null;
		case HOME:
		default:
			return false;
		}
	}

	private <C> void incrementCountOnMap(C key, Map<C, Integer> map) {
		map.put(key, map.getOrDefault(key, 0) + 1);
	}
}

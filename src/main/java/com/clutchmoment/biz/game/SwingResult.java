package com.clutchmoment.biz.game;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.card.Pitcher;
import com.clutchmoment.biz.enums.RangeOutcome;

public class SwingResult {

	private final int dieRoll;
	private final Batter batter;
	private final Pitcher pitcher;
	private final RangeOutcome outcome;

	public SwingResult(Batter batter, Pitcher pitcher, int dieRoll, RangeOutcome outcome) {
		this.batter = batter;
		this.pitcher = pitcher;
		this.dieRoll = dieRoll;
		this.outcome = outcome;
	}

	public Batter getBatter() {
		return batter;
	}

	public Pitcher getPitcher() {
		return pitcher;
	}

	public int getDieRoll() {
		return dieRoll;
	}

	public RangeOutcome getOutcome() {
		return outcome;
	}
}

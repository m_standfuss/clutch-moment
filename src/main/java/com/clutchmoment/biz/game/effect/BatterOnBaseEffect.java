package com.clutchmoment.biz.game.effect;

public interface BatterOnBaseEffect {

	public int modifyBatterOnBase(int currentOnBase);
}

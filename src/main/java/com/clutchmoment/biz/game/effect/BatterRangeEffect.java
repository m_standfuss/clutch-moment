package com.clutchmoment.biz.game.effect;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.game.PlayerRange;

public interface BatterRangeEffect {
	public PlayerRange modifyBatterRange(PlayerRange currentRange, Batter batter);
}

package com.clutchmoment.biz.game.effect;

public interface BatterSwingEffect {

	public int modifyBatterSwingRollEffect(int currentBatterSwingRoll);

}

package com.clutchmoment.biz.game.effect;

public interface CatcherDefenseEffect {

	public int modifyCatchersStolenBaseDefenseRating(int currentDefenseRating);
}

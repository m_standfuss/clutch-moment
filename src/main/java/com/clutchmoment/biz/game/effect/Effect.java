package com.clutchmoment.biz.game.effect;

import com.clutchmoment.biz.game.GameSituation;

public interface Effect {

	public boolean isActiveEffect(GameSituation gameSituation);

}

package com.clutchmoment.biz.game.effect;

public enum EffectAction {
	DO_NOTHING, DRAW_A_CARD, DRAW_TWO_CARDS, DISCARD_1_CARD;
}

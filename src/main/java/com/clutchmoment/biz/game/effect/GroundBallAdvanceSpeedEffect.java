package com.clutchmoment.biz.game.effect;

public interface GroundBallAdvanceSpeedEffect {

	public int modifyGroundBallAdvanceSpeed(int currentAdvanceSpeed);

}

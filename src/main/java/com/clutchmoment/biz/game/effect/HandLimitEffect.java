package com.clutchmoment.biz.game.effect;

import com.clutchmoment.biz.game.Scorecard;

public interface HandLimitEffect {

	public int modifyHandLimit(int currentHandLimit, boolean homeTeam, Scorecard scorecard);

}

package com.clutchmoment.biz.game.effect;

public interface MistakePitchEffect {

	public int modifyMistakePitchMax(int currentMistakePitchMax);
}

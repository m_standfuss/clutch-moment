package com.clutchmoment.biz.game.effect;

import com.clutchmoment.biz.game.GameSituation;

public interface MultiEffect {

	public Class<?> getPrimaryEffectType();

	public Class<?> getSecondEffectType();

	public boolean isSecondEffectActive(GameSituation gameSituation);

}

package com.clutchmoment.biz.game.effect;

import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.SwingResult;

public interface OutcomeResultActionEffect {

	public EffectAction getAction(GameSituation situation, boolean homeTeam, SwingResult previousSwingResult);
}

package com.clutchmoment.biz.game.effect;

import java.util.List;

import com.clutchmoment.biz.card.Player;

public interface OutfieldDefenseEffect {

	public int modifyOutfieldersDefenseRating(int currentDefenseRating, List<Player> outfielders);
}

package com.clutchmoment.biz.game.effect;

public interface PitcherCommandEffect {

	public int modifyPitcherCommand(int currentPitcherCommand);

}

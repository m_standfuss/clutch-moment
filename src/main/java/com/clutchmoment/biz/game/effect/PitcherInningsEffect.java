package com.clutchmoment.biz.game.effect;

public interface PitcherInningsEffect {

	public int modifyPitchersMaxInning(int currentMaxInnings);
}

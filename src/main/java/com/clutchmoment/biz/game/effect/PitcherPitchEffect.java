package com.clutchmoment.biz.game.effect;

public interface PitcherPitchEffect {
	
	public int modifyPitchersPitchRoll(int currentRoll);
	
}

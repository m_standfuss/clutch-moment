package com.clutchmoment.biz.game.effect;

import com.clutchmoment.biz.game.PlayerRange;

public interface PitcherRangeEffect {
	public PlayerRange modifyPitcherRange(PlayerRange currentRange);
}

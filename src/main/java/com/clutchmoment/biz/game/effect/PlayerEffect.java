package com.clutchmoment.biz.game.effect;

import org.javatuples.Pair;

import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.game.Scorecard;

public interface PlayerEffect {

	/**
	 * Returns the player that the effect will be applied to specifically, as well
	 * if the player is on the home team.
	 * 
	 * @param scorecard The scorecard of the current game.
	 * @return A tuple pair of a boolean if the player is on the home team and the
	 *         player to apply the effect to.
	 */
	public Pair<Boolean, Player> getPlayerToApplyTo(Scorecard scorecard);
}

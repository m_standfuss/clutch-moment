package com.clutchmoment.biz.game.effect;

public interface StolenBaseSpeedEffect {

	public int modifyStolenBaseSpeed(int currentStolenBaseSpeed);
}

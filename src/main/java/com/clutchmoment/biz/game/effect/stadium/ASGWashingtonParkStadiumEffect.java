package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.BatterOnBaseClutchEffect;
import com.clutchmoment.biz.game.effect.BatterSwingClutchEffect;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.PitcherCommandClutchEffect;
import com.clutchmoment.biz.game.effect.PitcherPitchClutchEffect;

public class ASGWashingtonParkStadiumEffect implements Effect, BatterSwingClutchEffect, BatterOnBaseClutchEffect,
		PitcherCommandClutchEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().getGamePosition().getInning() >= 8;
	}

}

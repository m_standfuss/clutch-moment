package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.MistakePitchEffect;

public class AtlantaParkStadiumEffect implements Effect, MistakePitchEffect {

	@Override
	public int modifyMistakePitchMax(int currentMistakePitchMax) {
		return Math.max(currentMistakePitchMax - 1, 1);
	}

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().getCurrentBatter().getTeam() != Team.ATL;
	}

}

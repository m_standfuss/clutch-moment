package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.enums.Handedness;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.PlayerRange;
import com.clutchmoment.biz.game.effect.BatterRangeEffect;
import com.clutchmoment.biz.game.effect.Effect;

public class BaltimoreYardsStadiumEffect implements Effect, BatterRangeEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		Handedness batterHand = gameSituation.getScorecard().getCurrentBatter().getHandedness();
		Handedness pitcherHand = gameSituation.getScorecard().getCurrentPitcher().getHandedness();
		boolean baltimorePitcher = gameSituation.getScorecard().getCurrentPitcher().getTeam() == Team.BAL;
		return !baltimorePitcher && (batterHand == Handedness.RIGHT
				|| (batterHand == Handedness.SWITCH && pitcherHand == Handedness.LEFT));
	}

	@Override
	public PlayerRange modifyBatterRange(PlayerRange currentRange, Batter batter) {
		return new PlayerRange(currentRange, RangeOutcome.HOMERUN, -1);
	}
}
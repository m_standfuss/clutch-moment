package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.enums.Icon;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.PlayerRange;
import com.clutchmoment.biz.game.effect.BatterRangeEffect;
import com.clutchmoment.biz.game.effect.Effect;

public class BigRedMachineStadiumEffect implements Effect, BatterRangeEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().getGamePosition().getInning() >= 7;
	}

	@Override
	public PlayerRange modifyBatterRange(PlayerRange currentRange, Batter batter) {
		int decreaseBy = -1;
		for (Icon icon : batter.getIcons()) {
			if (icon == Icon.MVP || icon == Icon.ALL_STAR) {
				decreaseBy = -2;
				break;
			}
		}
		return new PlayerRange(currentRange, RangeOutcome.HOMERUN, decreaseBy);
	}

}

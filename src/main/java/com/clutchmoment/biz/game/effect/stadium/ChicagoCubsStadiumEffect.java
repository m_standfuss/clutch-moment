package com.clutchmoment.biz.game.effect.stadium;

import java.util.Map.Entry;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.enums.Icon;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.game.Scorecard;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.PlayerRange;
import com.clutchmoment.biz.game.effect.BatterRangeEffect;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.HandLimitEffect;
import com.clutchmoment.biz.game.effect.MultiEffect;

public class ChicagoCubsStadiumEffect implements Effect, MultiEffect, BatterRangeEffect, HandLimitEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().getCurrentBatter().getSpeed() >= 20;
	}

	@Override
	public Class<?> getPrimaryEffectType() {
		return BatterRangeEffect.class;
	}

	@Override
	public PlayerRange modifyBatterRange(PlayerRange currentRange, Batter batter) {
		return new PlayerRange(currentRange, RangeOutcome.TRIPLE, RangeOutcome.HOMERUN);
	}

	@Override
	public boolean isSecondEffectActive(GameSituation gameSituation) {
		/*
		 * The hand limit effect is always active.
		 */
		return true;
	}

	@Override
	public Class<?> getSecondEffectType() {
		return HandLimitEffect.class;
	}

	@Override
	public int modifyHandLimit(int currentHandLimit, boolean homeTeam, Scorecard scorecard) {
		for (Entry<Player, Integer> entry : scorecard.getPlayerToHomeRunCount(homeTeam).entrySet()) {
			boolean hrIconBatter = false;
			for (Icon icon : entry.getKey().getIcons()) {
				if (icon == Icon.HOME_RUN) {
					hrIconBatter = true;
					break;
				}
			}
			if (hrIconBatter) {
				currentHandLimit += entry.getValue();
			}
		}
		return currentHandLimit;
	}
}

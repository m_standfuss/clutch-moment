package com.clutchmoment.biz.game.effect.stadium;

import static com.google.common.base.Preconditions.checkArgument;

import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.CatcherDefenseEffect;
import com.clutchmoment.biz.game.effect.Effect;

public class ClevelandFieldStadiumEffect implements Effect, CatcherDefenseEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		checkArgument(gameSituation.getPlayerAttemptingToAdvance() != null,
				"The player attempting to steal must be set on the game situation before making this effect.");
		return gameSituation.getPlayerAttemptingToAdvance().getTeam() != Team.CLE;
	}

	@Override
	public int modifyCatchersStolenBaseDefenseRating(int currentDefenseRating) {
		return Math.min(currentDefenseRating + 3, 10);
	}
}

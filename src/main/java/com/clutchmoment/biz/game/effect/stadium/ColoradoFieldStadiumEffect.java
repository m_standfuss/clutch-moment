package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.MistakePitchEffect;

public class ColoradoFieldStadiumEffect implements Effect, MistakePitchEffect {

	@Override
	public int modifyMistakePitchMax(int currentMistakePitchMax) {
		return Math.min(currentMistakePitchMax + 1, 3);
	}

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().getCurrentPitcher().getTeam() != Team.COL;
	}
}

package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.BatterSwingClutchEffect;
import com.clutchmoment.biz.game.effect.Effect;

public class DetroitParkStadiumEffect implements Effect, BatterSwingClutchEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		if (!gameSituation.getScorecard().isRunnerInScoringPosition()) {
			return false;
		}
		Batter batter = gameSituation.getScorecard().getCurrentBatter();
		if (batter.getTeam() != Team.DET) {
			return true;
		}
		return batter.getClutch() > 0;
	}

}

package com.clutchmoment.biz.game.effect.stadium;

import java.util.Map.Entry;

import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.enums.Icon;
import com.clutchmoment.biz.game.Scorecard;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.HandLimitEffect;

public class DingerTownStadiumEffect implements Effect, HandLimitEffect {

	@Override
	public int modifyHandLimit(int currentHandLimit, boolean homeTeam, Scorecard scorecard) {
		/*
		 * Primary effect: -1 from both Managers hand limit
		 */
		currentHandLimit--;

		/*
		 * Secondary effect: +1 hand limit for each HR from HR Icon Batters
		 */
		for (Entry<Player, Integer> entry : scorecard.getPlayerToHomeRunCount(homeTeam).entrySet()) {
			boolean hrIconBatter = false;
			for (Icon icon : entry.getKey().getIcons()) {
				if (icon == Icon.HOME_RUN) {
					hrIconBatter = true;
					break;
				}
			}
			if (hrIconBatter) {
				currentHandLimit += entry.getValue();
			}
		}
		return currentHandLimit;
	}

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		/*
		 * The hand limit effects are always active
		 */
		return true;
	}

}

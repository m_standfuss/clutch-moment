package com.clutchmoment.biz.game.effect.stadium;

import java.util.ArrayList;
import java.util.List;

import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.OutfieldDefenseEffect;

public class HoustonParkStadiumEffect implements Effect, OutfieldDefenseEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		List<Player> outfielders = new ArrayList<>();
		outfielders.add(gameSituation.getScorecard().getFieldingLineup().getLeftFielder());
		outfielders.add(gameSituation.getScorecard().getFieldingLineup().getCenterFielder());
		outfielders.add(gameSituation.getScorecard().getFieldingLineup().getRightFielder());
		return getNumberOfNonHoustonOutfielders(outfielders) > 0;
	}

	@Override
	public int modifyOutfieldersDefenseRating(int currentDefenseRating, List<Player> outfielders) {
		int nonHoustonOutfielders = getNumberOfNonHoustonOutfielders(outfielders);
		return Math.max(currentDefenseRating - nonHoustonOutfielders, 1);
	}

	private int getNumberOfNonHoustonOutfielders(List<Player> outfielders) {
		int cnt = 0;
		for (Player outfielder : outfielders) {
			if (outfielder.getTeam() != Team.HOU) {
				cnt++;
			}
		}
		return cnt;
	}

}

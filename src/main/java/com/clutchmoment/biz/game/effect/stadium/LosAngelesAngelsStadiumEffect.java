package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.SwingResult;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.EffectAction;
import com.clutchmoment.biz.game.effect.OutcomeResultActionEffect;

public class LosAngelesAngelsStadiumEffect implements Effect, OutcomeResultActionEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return true;
	}

	@Override
	public EffectAction getAction(GameSituation gameSituation, boolean homeTeam, SwingResult previousSwingResult) {
		boolean xbh = previousSwingResult.getOutcome() == RangeOutcome.DOUBLE
				|| previousSwingResult.getOutcome() == RangeOutcome.HOMERUN
				|| previousSwingResult.getOutcome() == RangeOutcome.TRIPLE;
		boolean offensiveTeam;
		if (homeTeam) {
			offensiveTeam = !gameSituation.getScorecard().getGamePosition().isTopOfInning();
		} else {
			offensiveTeam = gameSituation.getScorecard().getGamePosition().isTopOfInning();
		}
		if (xbh && offensiveTeam) {
			if (previousSwingResult.getBatter().getTeam() == Team.LA_A) {
				return EffectAction.DRAW_TWO_CARDS;
			} else {
				return EffectAction.DRAW_A_CARD;
			}
		}
		return EffectAction.DO_NOTHING;
	}

}

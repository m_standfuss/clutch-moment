package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.card.Pitcher;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.PitcherCommandClutchEffect;
import com.clutchmoment.biz.game.effect.PitcherPitchClutchEffect;

public class MiamiParkStadiumEffect implements Effect, PitcherCommandClutchEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		if (!gameSituation.getScorecard().isRunnerInScoringPosition()) {
			return false;
		}
		Pitcher pitcher = gameSituation.getScorecard().getCurrentPitcher();
		if (pitcher.getTeam() != Team.MIA) {
			return true;
		}
		return pitcher.getClutch() > 0;
	}
}

package com.clutchmoment.biz.game.effect.stadium;

import java.util.Arrays;
import java.util.List;

import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.GroundBallAdvanceSpeedEffect;
import com.clutchmoment.biz.game.player.Lineup;

public class MontrealStadiumEffect implements Effect, GroundBallAdvanceSpeedEffect {

	@Override
	public int modifyGroundBallAdvanceSpeed(int currentAdvanceSpeed) {
		return currentAdvanceSpeed + 5;
	}

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		Lineup fieldingLineup = gameSituation.getScorecard().getFieldingLineup();
		List<Player> infielders = Arrays.asList(fieldingLineup.getFirstBaseman(), fieldingLineup.getSecondBaseman(),
				fieldingLineup.getShortStop(), fieldingLineup.getThirdBaseman());
		for (Player infielder : infielders) {
			if (infielder.getTeam() == Team.WAS) {
				return true;
			}
		}
		return false;
	}
}

package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.PlayerRange;
import com.clutchmoment.biz.game.effect.Effect;

import com.clutchmoment.biz.game.effect.PitcherRangeEffect;

public class NewYorkMetsFieldStadiumEffect implements Effect, PitcherRangeEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().getCurrentBatter().getTeam() != Team.NY_N;
	}

	@Override
	public PlayerRange modifyPitcherRange(PlayerRange currentRange) {
		return new PlayerRange(currentRange, RangeOutcome.HOMERUN, RangeOutcome.DOUBLE);
	}

}

package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.PlayerRange;
import com.clutchmoment.biz.game.effect.BatterRangeEffect;
import com.clutchmoment.biz.game.effect.Effect;

public class NewYorkYankeeFieldStadiumEffect implements Effect, BatterRangeEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().getCurrentPitcher().getTeam() != Team.NY_A;
	}

	@Override
	public PlayerRange modifyBatterRange(PlayerRange currentRange, Batter batter) {
		return new PlayerRange(currentRange, RangeOutcome.HOMERUN, -1);
	}

}

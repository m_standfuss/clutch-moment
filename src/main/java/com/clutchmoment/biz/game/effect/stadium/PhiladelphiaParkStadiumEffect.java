package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.BatterSwingEffect;
import com.clutchmoment.biz.game.effect.Effect;

public class PhiladelphiaParkStadiumEffect implements Effect, BatterSwingEffect {

	@Override
	public int modifyBatterSwingRollEffect(int currentBatterSwingRoll) {
		return currentBatterSwingRoll + 1;
	}

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().isBattingTeamLosing()
				&& gameSituation.getScorecard().getCurrentPitcher().getTeam() != Team.PHI;
	}
}

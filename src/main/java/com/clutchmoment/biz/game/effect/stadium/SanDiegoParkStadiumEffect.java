package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.PitcherCommandEffect;

public class SanDiegoParkStadiumEffect implements Effect, PitcherCommandEffect {

	@Override
	public int modifyPitcherCommand(int currentPitcherRoll) {
		return currentPitcherRoll + 1;
	}

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().isRunnerInScoringPosition()
				&& gameSituation.getScorecard().getCurrentBatter().getTeam() != Team.SND;
	}
}

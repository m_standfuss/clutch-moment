package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.PlayerRange;
import com.clutchmoment.biz.game.effect.BatterRangeEffect;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.MultiEffect;
import com.clutchmoment.biz.game.effect.PitcherRangeEffect;

public class SanFranciscoParkStadiumEffect implements Effect, MultiEffect, BatterRangeEffect, PitcherRangeEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return true;
	}

	@Override
	public Class<?> getPrimaryEffectType() {
		return BatterRangeEffect.class;
	}

	@Override
	public PlayerRange modifyBatterRange(PlayerRange currentRange, Batter batter) {
		return new PlayerRange(currentRange, true, RangeOutcome.WALK, RangeOutcome.SINGLE);
	}

	@Override
	public boolean isSecondEffectActive(GameSituation gameSituation) {
		return gameSituation.getScorecard().getCurrentBatter().getTeam() == Team.SNF;
	}

	@Override
	public Class<?> getSecondEffectType() {
		return PitcherRangeEffect.class;
	}

	@Override
	public PlayerRange modifyPitcherRange(PlayerRange currentRange) {
		return new PlayerRange(currentRange, true, RangeOutcome.WALK, RangeOutcome.SINGLE);
	}
}

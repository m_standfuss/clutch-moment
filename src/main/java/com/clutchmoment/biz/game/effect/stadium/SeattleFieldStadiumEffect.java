package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.PitcherCommandEffect;

public class SeattleFieldStadiumEffect implements Effect, PitcherCommandEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().isFieldingTeamLosing()
				&& gameSituation.getScorecard().getCurrentBatter().getTeam() != Team.SEA;
	}

	@Override
	public int modifyPitcherCommand(int currentPitcherRoll) {
		return currentPitcherRoll + 1;
	}
}

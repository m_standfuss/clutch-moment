package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.PlayerRange;
import com.clutchmoment.biz.game.effect.BatterRangeEffect;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.MultiEffect;
import com.clutchmoment.biz.game.effect.PitcherRangeEffect;

public class TampaBayFieldStadiumEffect implements Effect, BatterRangeEffect, PitcherRangeEffect, MultiEffect {

	@Override
	public Class<?> getPrimaryEffectType() {
		return PitcherRangeEffect.class;
	}

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return true;
	}

	@Override
	public PlayerRange modifyBatterRange(PlayerRange currentRange, Batter batter) {
		return new PlayerRange(currentRange, true, RangeOutcome.FLYBALL, RangeOutcome.SINGLE);
	}

	@Override
	public Class<?> getSecondEffectType() {
		return BatterRangeEffect.class;
	}

	@Override
	public boolean isSecondEffectActive(GameSituation gameSituation) {
		return gameSituation.getScorecard().getCurrentBatter().getTeam() == Team.TPB;
	}

	@Override
	public PlayerRange modifyPitcherRange(PlayerRange currentRange) {
		return new PlayerRange(currentRange, true, RangeOutcome.FLYBALL, RangeOutcome.SINGLE);
	}
}
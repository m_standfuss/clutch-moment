package com.clutchmoment.biz.game.effect.stadium;

import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.enums.Team;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.PlayerRange;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.PitcherRangeEffect;

public class TorontoCentreStadiumEffect implements Effect, PitcherRangeEffect {

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		return gameSituation.getScorecard().getCurrentPitcher().getTeam() != Team.TOR;
	}

	@Override
	public PlayerRange modifyPitcherRange(PlayerRange currentRange) {
		return new PlayerRange(currentRange, true, RangeOutcome.GROUNDBALL, RangeOutcome.SINGLE);
	}
}

package com.clutchmoment.biz.game.effect.strategy;

import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.enums.LineupPosition;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.game.DiceRoller;
import com.clutchmoment.biz.game.Scorecard;
import com.clutchmoment.biz.game.effect.EffectAction;

public abstract class DefensivePlayStrategy extends Strategy {

	@Override
	public boolean isStrategyTriggered(Scorecard scorecard, RangeOutcome currentPlay) {
		return currentPlay == triggerPlay();
	}

	public EffectAction preconditionAction() {
		/*
		 * Default no extra precondition action, though many need to discard a card
		 * after playing.
		 */
		return null;
	}

	public RangeOutcome makeDefensivePlay(RangeOutcome currentPlay, Scorecard scorecard, DiceRoller dieRoller) {
		Player playerMakingDefensivePlay = scorecard.getFieldingLineup().getPlayer(getPositionMakingPlay());
		int defensiveRating = playerMakingDefensivePlay.getDefense();
		int roll = dieRoller.getRegularDiceRoll();
		boolean playMade = defensiveRating + roll >= 20;
		if (playMade) {
			return newOutcome();
		}
		return currentPlay;
	}

	public abstract LineupPosition getPositionMakingPlay();

	public abstract RangeOutcome triggerPlay();

	public abstract RangeOutcome newOutcome();
}

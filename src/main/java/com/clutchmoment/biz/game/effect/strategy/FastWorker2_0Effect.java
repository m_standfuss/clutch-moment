package com.clutchmoment.biz.game.effect.strategy;

import java.util.List;

import org.javatuples.Pair;

import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.game.Scorecard;
import com.clutchmoment.biz.game.effect.PitcherInningsEffect;
import com.clutchmoment.biz.game.effect.PlayerEffect;

public class FastWorker2_0Effect extends Strategy implements PitcherInningsEffect, PlayerEffect {

	@Override
	public boolean isStrategyTriggered(Scorecard scorecard, RangeOutcome currentPlay) {
		List<RangeOutcome> lastPlays = scorecard.getLastPlays(3);
		for (RangeOutcome outcome : lastPlays) {
			if (!outcome.isOut()) {
				return false;
			}
		}
		/*
		 * All the most recent outcomes were outs so sanity check to make sure the game
		 * has had at least 3 at bats.
		 */
		return lastPlays.size() == 3;
	}

	@Override
	public int modifyPitchersMaxInning(int currentMaxInnings) {
		return ++currentMaxInnings;
	}

	@Override
	public Pair<Boolean, Player> getPlayerToApplyTo(Scorecard scorecard) {
		/*
		 * This is under the assumption that the trigger has happened so the last
		 * pitcher will be the pitcher for the team that just ended the inning.
		 */
		if (scorecard.getGamePosition().isTopOfInning()) {
			return Pair.with(false, scorecard.getOtherTeamsPitcher());
		} else {
			return Pair.with(true, scorecard.getOtherTeamsPitcher());
		}
	}

}

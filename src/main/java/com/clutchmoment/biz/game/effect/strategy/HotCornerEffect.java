package com.clutchmoment.biz.game.effect.strategy;

import com.clutchmoment.biz.enums.LineupPosition;
import com.clutchmoment.biz.enums.RangeOutcome;

public class HotCornerEffect extends DefensivePlayStrategy {

	@Override
	public RangeOutcome triggerPlay() {
		return RangeOutcome.SINGLE;
	}

	@Override
	public RangeOutcome newOutcome() {
		return RangeOutcome.GROUNDBALL;
	}

	@Override
	public LineupPosition getPositionMakingPlay() {
		return LineupPosition.THIRD;
	}

}

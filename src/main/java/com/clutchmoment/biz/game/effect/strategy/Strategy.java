package com.clutchmoment.biz.game.effect.strategy;

import com.clutchmoment.biz.enums.RangeOutcome;
import com.clutchmoment.biz.game.GameSituation;
import com.clutchmoment.biz.game.Scorecard;
import com.clutchmoment.biz.game.effect.Effect;

public abstract class Strategy implements Effect {

	public abstract boolean isStrategyTriggered(Scorecard scorecard, RangeOutcome currentPlay);

	@Override
	public boolean isActiveEffect(GameSituation gameSituation) {
		/*
		 * All strategies once triggered and played are always in effect
		 */
		return true;
	}
}

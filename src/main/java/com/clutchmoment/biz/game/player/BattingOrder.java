package com.clutchmoment.biz.game.player;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;

import com.clutchmoment.biz.card.Batter;

public class BattingOrder {

	private final List<Batter> batters;

	public BattingOrder(List<Batter> batters) {
		checkArgument(batters != null, "The Batters for the batting order cannot be null.");
		checkArgument(!batters.contains(null), "The Batters list cannot contain null.");
		checkArgument(batters.size() == 9, "The Batters list must contain 9 Batters.");
		this.batters = new ArrayList<>(batters);
	}

	public List<Batter> getBattingOrder(){
		return new ArrayList<>(batters);
	}

	public Batter getBatter(int position) {
		checkArgument(position >= 1 && position <= 9, "The position requested must be between 1-9.");
		return batters.get(position - 1);
	}

	public void replaceBatter(Batter toReplace, Batter replaceWith) {
		for (int i = 0; i < batters.size(); i++) {
			if (toReplace == batters.get(i)) {
				batters.set(i, replaceWith);
				return;
			}
		}

		throw new IllegalStateException("Unable to find the batter to replace.");
	}
}

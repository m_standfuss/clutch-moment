package com.clutchmoment.biz.game.player;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.clutchmoment.biz.card.Strategy;

public class GameCompetitor {

	private final long userId;
	private final PlayerDeck deck;

	private final List<Strategy> hand;

	public GameCompetitor(long userId, PlayerDeck deck) {
		this.userId = userId;
		this.deck = deck;
		this.hand = new ArrayList<>();
	}

	public List<Strategy> getStrategyHand() {
		return new ArrayList<>(hand);
	}

	public Strategy drawNextStrategy() {
		Strategy nextStrategy = deck.getNextStrategy();
		hand.add(nextStrategy);
		return nextStrategy;
	}

	public void discardStrategy(Strategy strategy) {
		hand.remove(strategy);
	}

	public PlayerDeck getPlayerDeck() {
		return deck;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		GameCompetitor that = (GameCompetitor) o;
		return userId == that.userId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(userId);
	}
}

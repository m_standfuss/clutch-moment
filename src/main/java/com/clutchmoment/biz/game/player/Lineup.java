package com.clutchmoment.biz.game.player;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.card.Pitcher;
import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.enums.LineupPosition;

public class Lineup {

	private BattingOrder battingOrder;
	private Set<Player> players;
	private HashMap<LineupPosition, Player> positionToPlayer;

	public Lineup() {
		players = new HashSet<>();
		positionToPlayer = new HashMap<>();
	}

	public boolean isFullLineup() {
		for (LineupPosition position : LineupPosition.values()) {
			if (positionToPlayer.get(position) == null) {
				return false;
			}
		}
		if (battingOrder == null) {
			return false;
		}
		return true;
	}

	public boolean contains(Player player) {
		return players.contains(player);
	}

	public void makeSubstitution(Player comingIn, Player goingOut) {
		checkState(players.contains(comingIn), "The player substituting for is not currently in the lineup.");
		players.remove(goingOut);
		players.add(comingIn);
		for (Entry<LineupPosition, Player> entry : positionToPlayer.entrySet()) {
			if (entry.getValue().equals(goingOut)) {
				entry.setValue(comingIn);
				break;
			}
		}

		if (comingIn instanceof Batter) {
			battingOrder.replaceBatter((Batter) goingOut, (Batter) goingOut);
		}
	}

	public int getOutfieldDefensiveRating() {
		int outfieldRating = 0;
		outfieldRating += getLeftFielder().getDefense();
		outfieldRating += getCenterFielder().getDefense();
		outfieldRating += getRightFielder().getDefense();
		return outfieldRating;
	}

	public int getInfieldDefensiveRating() {
		int infieldRating = 0;
		infieldRating += getThirdBaseman().getDefense();
		infieldRating += getShortStop().getDefense();
		infieldRating += getSecondBaseman().getDefense();
		infieldRating += getFirstBaseman().getDefense();
		return infieldRating;
	}

	public int getBatteryDefensiveRating() {
		int batteryRating = 0;
		batteryRating += getPitcher().getDefense();
		batteryRating += getCatcher().getDefense();
		return batteryRating;
	}

	public BattingOrder getBattingOrder() {
		return battingOrder;
	}

	public void setBattingOrder(BattingOrder battingOrder) {
		this.battingOrder = battingOrder;
	}

	public void setPlayer(LineupPosition position, Player player) {
		checkArgument(position != null, "The position to set the player to must be provided.");
		checkArgument(player != null, "The player to set must be not null.");
		if (position == LineupPosition.PITCHER) {
			if (!(player instanceof Pitcher)) {
				throw new IllegalArgumentException("Only pitchers can be played in the pitcher position.");
			}
		} else {
			if (!(player instanceof Batter)) {
				throw new IllegalArgumentException("Only batters can be played in a non-pitcher position.");
			}
		}
		Player currentPlayer = positionToPlayer.get(position);
		if (currentPlayer != null) {
			players.remove(currentPlayer);
		}
		positionToPlayer.put(position, player);
	}

	public Player getPlayer(LineupPosition position) {
		return positionToPlayer.get(position);
	}

	public void setPitcher(Pitcher pitcher) {
		setPlayer(LineupPosition.PITCHER, pitcher);
	}

	public Pitcher getPitcher() {
		return (Pitcher) positionToPlayer.get(LineupPosition.PITCHER);
	}

	public Batter getCatcher() {
		return (Batter) positionToPlayer.get(LineupPosition.CATCHER);
	}

	public void setCatcher(Batter catcher) {
		setPlayer(LineupPosition.CATCHER, catcher);
	}

	public Batter getFirstBaseman() {
		return (Batter) positionToPlayer.get(LineupPosition.FIRST);
	}

	public void setFirstBaseman(Batter firstBaseman) {
		setPlayer(LineupPosition.FIRST, firstBaseman);
	}

	public Batter getSecondBaseman() {
		return (Batter) positionToPlayer.get(LineupPosition.SECOND);
	}

	public void setSecondBaseman(Batter secondBaseman) {
		setPlayer(LineupPosition.SECOND, secondBaseman);
	}

	public Batter getThirdBaseman() {
		return (Batter) positionToPlayer.get(LineupPosition.THIRD);
	}

	public void setThirdBaseman(Batter thirdBaseman) {
		setPlayer(LineupPosition.THIRD, thirdBaseman);
	}

	public Batter getShortStop() {
		return (Batter) positionToPlayer.get(LineupPosition.SS);
	}

	public void setShortStop(Batter shortStop) {
		players.remove(getShortStop());
		positionToPlayer.put(LineupPosition.SS, shortStop);
	}

	public Batter getLeftFielder() {
		return (Batter) positionToPlayer.get(LineupPosition.LF);
	}

	public void setLeftFielder(Batter leftFielder) {
		setPlayer(LineupPosition.LF, leftFielder);
	}

	public Batter getCenterFielder() {
		return (Batter) positionToPlayer.get(LineupPosition.CF);
	}

	public void setCenterFielder(Batter centerFielder) {
		setPlayer(LineupPosition.CF, centerFielder);
	}

	public Batter getRightFielder() {
		return (Batter) positionToPlayer.get(LineupPosition.RF);
	}

	public void setRightFielder(Batter rightFielder) {
		setPlayer(LineupPosition.RF, rightFielder);
	}

	public Batter getDesignatedBatter() {
		return (Batter) positionToPlayer.get(LineupPosition.DH);
	}

	public void setDesignatedBatter(Batter designatedBatter) {
		setPlayer(LineupPosition.DH, designatedBatter);
	}

}

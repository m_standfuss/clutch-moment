package com.clutchmoment.biz.game.player;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.card.Stadium;
import com.clutchmoment.biz.card.Strategy;

public class PlayerDeck {

	private final Set<Player> playerCards;
	private final Set<Strategy> strategyCards;
	private final Stack<Strategy> strategyCardStack;
	private final Stadium stadium;

	public PlayerDeck(List<Player> playerCards, List<Strategy> strategyCards, Stadium stadium) {
		checkArgument(playerCards != null, "The player cards cannot be null.");
		checkArgument(strategyCards != null, "The strategy cards cannot be null.");
		this.playerCards = new HashSet<>(playerCards);
		this.strategyCards = new HashSet<>(strategyCards);
		this.strategyCardStack = new Stack<>();
		strategyCardStack.addAll(shuffleCollection(strategyCards));
		this.stadium = stadium;
	}

	public List<Player> getPlayers() {
		return new ArrayList<>(playerCards);
	}

	public Strategy getNextStrategy() {
		return strategyCardStack.pop();
	}

	public Stadium getStadium() {
		return stadium;
	}

	private <T> List<T> shuffleCollection(Collection<T> collection) {
		List<T> list = new ArrayList<>(collection);
		Collections.shuffle(list);
		return list;
	}
}

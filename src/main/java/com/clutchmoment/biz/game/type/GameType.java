package com.clutchmoment.biz.game.type;

public interface GameType {

	public int getRosterSize();

	public int getSalaryMax();

	public int getMaxBatters();

	public int getMaxBench();

	public int getMaxStartingPitchers();

	public int getMaxReliefPitchers();

	public int getMaxClosers();

	public int getMaxSalary();
}

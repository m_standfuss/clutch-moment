package com.clutchmoment.data.access;

import static com.google.common.base.Preconditions.checkArgument;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * A wrapper around a hibernate session to make it auto closeable enabled.
 * 
 * @author Mike
 */
class CloseableSession implements AutoCloseable {

	private static final Logger logger = LogManager.getLogger(CloseableSession.class);

	private final Session session;
	private final Transaction transaction;

	/**
	 * Creates a new closeable session wrapper for the provided session.
	 * 
	 * @param session The session.
	 * @throws IllegalArgumentException If the session is null.
	 */
	CloseableSession(Session session) {
		checkArgument(session != null, "The session cannot be null.");
		this.session = session;
		transaction = session.beginTransaction();
	}

	/**
	 * Gets the session backing the closable session.
	 * 
	 * @return The session, can never be null.
	 */
	Session getSession() {
		return session;
	}

	/**
	 * Closes the session and any transactions that were started.
	 */
	@Override
	public void close() {
		try {
			transaction.commit();
		} catch (Exception e) {
			logger.error("An error occured attempting to commit transaction, instead will be rolled back.", e);
			transaction.rollback();
		}

		try {
			session.close();
		} catch (Exception e) {
			logger.error("An error occured while attempting to close the session.", e);
		}
	}
}

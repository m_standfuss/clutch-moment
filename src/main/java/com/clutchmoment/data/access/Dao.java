package com.clutchmoment.data.access;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import com.clutchmoment.data.model.DataEntity;
import com.google.common.collect.Lists;

/**
 * The abstract data access class to handle anything that can be made generic
 * enough to benefit all concrete data access classes tied to a specific model.
 *
 * @author Mike
 * @param <T> The model for data access.
 */
public abstract class Dao<T extends DataEntity> {

	/**
	 * The maximum number of in parameters that can be sent into a MySQL query.
	 */
	public static final int MAX_IN_PARAMS = 2000;

	private static final Logger logger = LogManager.getLogger(Dao.class);

	private static final String DATABASE_NAME = "vicarius";
	private static final String PORT = "3306";

	private static final String QUERY_TEST = "select 0";
	private static final String QUERY_BY_IDS = "from %s where id IN :ID_LIST";
	private static final String QUERY_ACTIVE = "from %s where activeInd = 1";

	private static String hostName = null;
	private static String username = null;
	private static char[] password = null;
	private static boolean credsChanged = true;
	private static final Object sessionFactoryLock = new Object();
	private static SessionFactory sessionFactory = null;

	private final Class<T> entityClass;

	/**
	 * Sets the url of the datastore to connect to.
	 *
	 * @param hostName The hostname to connect to.
	 * @throws IllegalArgumentException If the hostname is null or empty.
	 */
	public static final void setHostName(String hostName) {
		logger.debug("Setting the database host name to " + hostName);
		checkArgument(hostName != null, "The hostname cannot be null.");
		checkArgument(!hostName.trim().isEmpty(), "The hostname cannot be empty.");
		Dao.hostName = hostName;
		credsChanged = true;
	}

	public static final void setUsername(String username) {
		logger.debug("Setting the database username to " + username);
		checkArgument(username != null, "The username cannot be null.");
		checkArgument(!username.trim().isEmpty(), "The username cannot be empty.");
		Dao.username = username;
		credsChanged = true;
	}

	/**
	 * Sets the password of the datastore to connect to.
	 * 
	 * @param password The password.
	 * @throws IllegalArgumentException If the password is null or empty.
	 */
	public static final void setPassword(char[] password) {
		logger.debug("Setting the database password");
		checkArgument(password != null, "The password cannot be null.");
		checkArgument(password.length != 0, "The password cannot be empty.");
		Dao.password = password;
		credsChanged = true;
	}

	/**
	 * Closes all open connections allowing them to terminate properly.
	 *
	 * @throws IllegalStateException If the session factory was never created.
	 * @throws HibernateException    Indicates a problem closing connections.
	 */
	public static void closeConnections() throws HibernateException {
		logger.debug("Starting closeConnections.");
		if (sessionFactory == null) {
			throw new IllegalStateException("The session factory was not created.");
		}
		sessionFactory.close();
	}

	/**
	 * Attempts to create a session to the data store. If anything goes wrong
	 * swallows the problem and returns false.
	 *
	 * @return If a connection can successfully be made to the data store.
	 */
	public static boolean testConnection() {
		logger.debug("Testing the hibernate connection to the database.");

		try (CloseableSession session = getOpenSession()) {
			session.getSession().createNativeQuery(QUERY_TEST).getSingleResult();
		} catch (final Exception e) {
			logger.error("Cannot make a successful connection to the data base.", e);
			return false;
		}
		return true;
	}

	/**
	 * Sets the session factory for all the data access objects. This should be set
	 * automatically when the first request is issued after any database credentials
	 * are set. This is here for dependency injection purposes.
	 *
	 * @param sessionFactory The session factory to set.
	 * @throws IllegalArgumentException If the session factory is null.
	 */
	public static void setSessionFactory(SessionFactory sessionFactory) {
		checkArgument(sessionFactory != null, "The session factory cannot be null.");
		Dao.sessionFactory = sessionFactory;
		credsChanged = false;
	}

	/**
	 * Gets a new session from the data store and opens it with the current session
	 * configuration settings. Caller of this function is responsible for closing
	 * the session properly. If any configurations have changed since the last
	 * session was created the session factory will be flushed and recreated to
	 * reflect those changes.
	 *
	 * @return A new open session.
	 */
	protected static CloseableSession getOpenSession() {
		logger.debug("Starting getOpenSession.");
		if (credsChanged) {
			logger.debug("The connection credentials have changed since the last session was generated. "
					+ "Recreating the session factory.");
			createSessionFactory();
		}
		CloseableSession session = new CloseableSession(sessionFactory.getCurrentSession());
		return session;
	}

	/**
	 * Takes a list and partitions it up into a lists of acceptable length (
	 * {@value #MAX_IN_PARAMS}) for using in an in parameter query.
	 *
	 * @param collection The collection to chunk up.
	 * @return The resulting list of lists.
	 * @throws IllegalArgumentException If list is null.
	 */
	protected static <E> List<List<E>> partitionInParameterList(Collection<E> collection) {
		checkArgument(collection != null, "The list cannot be null.");
		List<E> list;
		if (collection instanceof List) {
			list = (List<E>) collection;
		} else {
			list = Lists.newArrayList(collection);
		}
		return Lists.partition(list, MAX_IN_PARAMS);
	}

	/**
	 * Uses the current url to create a session factory and store it in static
	 * state.
	 *
	 * @throws IllegalStateException If the hostname, username or password is not
	 *                               set.
	 */
	private static void createSessionFactory() throws IllegalStateException {
		logger.debug("Attempting to make a new session factory.");
		checkState(hostName != null, "The hostname of the database was not set.");
		checkState(username != null, "The username of the database was not set.");
		checkState(password != null, "The password of the database was not set.");

		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure()
				.applySetting("hibernate.connection.url", getUrl())
				.applySetting("hibernate.connection.username", username)
				.applySetting("hibernate.connection.password", new String(password)).build();

		synchronized (sessionFactoryLock) {
			try {
				sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
				credsChanged = false;
			} catch (Exception e) {
				logger.error("An error occured while attempting to make a session factory.", e);
				StandardServiceRegistryBuilder.destroy(registry);
			}
		}
	}

	/**
	 * Gets the formatted MySQL database url connection string.
	 *
	 * @return The url connection string.
	 */
	private static String getUrl() {
		return "jdbc:mysql://" + hostName + ":" + PORT + "/" + DATABASE_NAME;
	}

	/**
	 * Default constructor.
	 */
	protected Dao() {
		entityClass = getEntityClass();
	}

	/**
	 * Inserts or updates the entity into the data store based on if the primary key
	 * is set or not. Note all data entities will have their update date time set to
	 * now and the active ind set to true.
	 *
	 * @param entity The entity to persist.
	 * @throws IllegalArgumentException If the entity is null.
	 */
	public void persist(T entity) {
		logger.debug("Starting persist for the entity " + entity);
		checkArgument(entity != null, "The entity cannot be null.");
		entity.setUpdtDttm(new Date());
		entity.setActiveInd(true);

		_persist(entity);
	}

	/**
	 * Removes the model from the data store.
	 *
	 * @param entity The entity to remove.
	 * @throws IllegalArgumentException If the entity is null.
	 */
	public void remove(T entity) {
		logger.debug("Starting remove for the entity " + entity);
		checkArgument(entity != null, "The entity cannot be null.");
		logger.debug("Removing entity " + entity);
		try (CloseableSession session = getOpenSession()) {
			session.getSession().delete(entity);
		}
	}

	/**
	 * Inactivates the entity in the data store. Used for logical deletion of a
	 * model instead of the remove method.</br>
	 * </br>
	 * <i>Note:</i> this will not make any cascading updates to child models.
	 *
	 * @param entity The entity to logically delete.
	 */
	public void inactivate(T entity) {
		logger.debug("Starting inactivate for the entity " + entity);
		checkArgument(entity != null, "The entity cannot be null.");
		entity.setUpdtDttm(new Date());
		entity.setActiveInd(false);
		_persist(entity);
	}

	/**
	 * Inactivates an entity based on the primary key. First gets the entity with
	 * {@link #findById(Serializable)} then calls {@link #inactivate(DataEntity)}
	 * with the result, if not null.
	 *
	 * @param id The id of the entity to inactivate.
	 */
	public void inactivate(Serializable id) {
		logger.debug("Starting inactivate for id = " + id);
		checkArgument(id != null, "The id cannot be null.");
		final T entity = findById(id);
		if (entity == null) {
			logger.error("Unable to find the entity with id = + " + id + " unable to inactivate.");
			return;
		}
		inactivate(entity);
	}

	/**
	 * Reactivates an entity. First gets the entity with
	 * {@link #findById(Serializable)} then updates the active indicator and
	 * persists it.
	 *
	 * @param entity The entity to reactivate.
	 * @throws IllegalArgumentException If the entity is null.
	 */
	public void reactivate(T entity) {
		logger.debug("Starting reactivate for entity " + entity);
		checkArgument(entity != null, "The entity cannot be null.");
		entity.setUpdtDttm(new Date());
		entity.setActiveInd(true);
		_persist(entity);
	}

	/**
	 * Reactivates an entity based on its primary key. First gets the entity with
	 * {@link #findById(Serializable)} then calls {@link #reactivate(DataEntity)}
	 * with the result, if not null.
	 *
	 * @param id The primary key of the entity.
	 * @throws IllegalArgumentException If id is null.
	 */
	public void reactivate(Serializable id) {
		logger.debug("Starting reactivate for id = " + id);
		checkArgument(id != null, "The id cannot be null.");
		T entity = findById(id);
		if (entity == null) {
			logger.error("Unable to find the entity with id = " + id + " unable to reactivate.");
			return;
		}
		reactivate(entity);
	}

	/**
	 * Gets a entity model based on the primary key from the data store.
	 *
	 * @param id The primary key.
	 * @throws IllegalArgumentException If the id is null.
	 * @return The entity if found, null if not.
	 */
	public T findById(Serializable id) {
		logger.debug("Starting findById for id = " + id);
		checkArgument(id != null, "The id cannot be null.");

		try (CloseableSession session = getOpenSession()) {
			return session.getSession().get(entityClass, id);
		}
	}

	/**
	 * Gets a list of entity models based on a set of primary keys. If an empty list
	 * of identifiers is passed in an empty list of entities will be returned.</br>
	 * </br>
	 * Porting this over to hibernate 5.2 depricates the existing Query class, I am
	 * currently unable to find a replacement method to the
	 * {@link Query#setParameterList(String, Collection)} method.
	 *
	 * @param idList The list of identifiers.
	 * @return The result list.
	 * @throws IllegalArgumentException if the idList is null.
	 */
	@SuppressWarnings("deprecation")
	public List<T> findByIds(Collection<? extends Serializable> idList) {
		logger.debug("Starting findByIdList for ids = " + idList);
		checkArgument(idList != null, "The id list cannot be null.");
		if (idList.isEmpty()) {
			return Lists.newArrayList();
		}
		List<T> results = Lists.newArrayList();
		for (List<? extends Serializable> chunk : partitionInParameterList(idList)) {
			try (CloseableSession session = getOpenSession()) {
				String query = String.format(QUERY_BY_IDS, entityClass.getName());
				org.hibernate.Query<T> q = session.getSession().createQuery(query, entityClass)
						.setParameterList("ID_LIST", chunk);
				results.addAll(q.getResultList());
			}
		}
		return results;
	}

	/**
	 * Gets all of the active models.
	 *
	 * @return The list of active models.
	 */
	public List<T> findActive() {
		logger.debug("Finding active rows for entity class = " + entityClass + ".");
		try (CloseableSession session = getOpenSession()) {
			String query = String.format(QUERY_ACTIVE, entityClass.getName());
			Query<T> q = session.getSession().createQuery(query, entityClass);
			return q.getResultList();
		}
	}

	/**
	 * Casts the generic list into a model specific list. Allows for the compiler
	 * warning to be swallowed in a single place rather than in each query.
	 *
	 * @param resultList The result list to cast.
	 * @return The casted list.
	 */
	@SuppressWarnings("unchecked")
	protected List<T> getResults(List<?> resultList) {
		return (List<T>) resultList;
	}

	/**
	 * Gets a single model specific result from a generic list of results. If the
	 * list is empty null will be returned else the first element on the list
	 * provided. Allows for the compiler warning to be swallowed in a single place
	 * rather than in each query/dao method.
	 *
	 * @param resultList The result list.
	 * @return The single result.
	 */
	@SuppressWarnings("unchecked")
	protected T getSingleResult(List<?> resultList) {
		if (resultList.isEmpty()) {
			return null;
		}
		return (T) resultList.get(0);
	}

	/**
	 * Private implementation of the persist method which does not set any of the
	 * fields prior to being written to the datastore. Allows methods like
	 * inactivate to use same logic as persist but not have the active ind set back
	 * to true.
	 *
	 * @param entity The entity to persist. Should not be null but unchecked.
	 */
	private void _persist(T entity) {
		try (CloseableSession session = getOpenSession()) {
			session.getSession().saveOrUpdate(entity);
		}
	}

	/**
	 * Uses reflection to get the type parameter of the Dao class.
	 */
	@SuppressWarnings("unchecked")
	private Class<T> getEntityClass() {
		final ParameterizedType genericSuperClass = (ParameterizedType) getClass().getGenericSuperclass();
		if (genericSuperClass.getActualTypeArguments().length == 0) {
			throw new IllegalArgumentException("The Dao must be implemented with a type parameter.");
		}
		logger.debug("Entity type of jpa " + genericSuperClass.getActualTypeArguments()[0]);
		return (Class<T>) genericSuperClass.getActualTypeArguments()[0];
	}
}

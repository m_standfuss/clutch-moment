package com.clutchmoment.data.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "batter_card")
public class BatterCard implements DataEntity {

	@Column(name = "card_id")
	@Id
	private long cardId;

	@Column(name = "on_base")
	private int onBase;

	@Column(name = "speed")
	private int speed;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return getCardId();
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public int getOnBase() {
		return onBase;
	}

	public void setOnBase(int onBase) {
		this.onBase = onBase;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

}

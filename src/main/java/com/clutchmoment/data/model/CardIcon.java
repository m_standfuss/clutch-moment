package com.clutchmoment.data.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "card_icon")
public class CardIcon implements DataEntity {

	@Column(name = "card_icon_id")
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long cardIconId;

	@Column(name = "card_id")
	private long cardId;

	@Column(name = "icon")
	private String icon;
	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return getCardIconId();
	}

	public long getCardIconId() {
		return cardIconId;
	}

	public void setCardIconId(long cardIconId) {
		this.cardIconId = cardIconId;
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}
}

package com.clutchmoment.data.model;

import java.io.Serializable;
import java.util.Date;

/**
 * An interface so that the update columns can be set generically before each
 * update/insert.
 * 
 * @author Mike
 *
 */
public interface DataEntity {

	/**
	 * Gets the primary key of the entity. Primary keys must be Serializable.
	 * 
	 * @return The primary key.
	 */
	public Serializable getPrimaryKey();

	/**
	 * Sets the update date and time of the last time this row was updated.
	 * 
	 * @param updtDttm The date of the update/insert.
	 */
	public void setUpdtDttm(Date updtDttm);

	/**
	 * Sets the active indicator of the entity.
	 * 
	 * Note if using to logically delete a row does not do any of the same cascading
	 * as a foreign key would. Child rows must be deleted or logically deleted
	 * manually.
	 * 
	 * @param activeInd The active indicator of the row.
	 */
	public void setActiveInd(boolean activeInd);
}

package com.clutchmoment.data.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pitcher_card")
public class PitcherCard implements DataEntity {

	@Column(name = "card_id")
	@Id
	private long cardId;

	@Column(name = "comand")
	private int command;

	@Column(name = "inning_limit")
	private int inningLimit;

	@Column(name = "mistake_pitch")
	private int mistakePitch;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return getCardId();
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public int getCommand() {
		return command;
	}

	public void setCommand(int command) {
		this.command = command;
	}

	public int getInningLimit() {
		return inningLimit;
	}

	public void setInningLimit(int inningLimit) {
		this.inningLimit = inningLimit;
	}

	public int getMistakePitch() {
		return mistakePitch;
	}

	public void setMistakePitch(int mistakePitch) {
		this.mistakePitch = mistakePitch;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

}

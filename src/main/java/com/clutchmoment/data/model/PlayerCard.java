package com.clutchmoment.data.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "card")
public class PlayerCard implements DataEntity {

	@Column(name = "card_id")
	@Id
	private long cardId;

	@Column(name = "position")
	private String position;

	@Column(name = "team")
	private String team;

	@Column(name = "handedness")
	private String handedness;

	@Column(name = "salary")
	private int salary;

	@Column(name = "defense")
	private int defense;

	@Column(name = "clutch")
	private int clutch;

	@Column(name = "matchup_handedness")
	private String matchupHandedness;

	@Column(name = "matchup_qualifier")
	private int matchupQualifier;

	@Column(name = "chart_strikeout")
	private int chartStrikeout;

	@Column(name = "chart_groundball")
	private int chartGroundball;

	@Column(name = "chart_flyball")
	private int chartFlyball;

	@Column(name = "chart_walk")
	private int chartWalk;

	@Column(name = "chart_single")
	private int chartSingle;

	@Column(name = "chart_double")
	private int chartDouble;

	@Column(name = "chart_triple")
	private int chartTriple;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return getCardId();
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getHandedness() {
		return handedness;
	}

	public void setHandedness(String handedness) {
		this.handedness = handedness;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getClutch() {
		return clutch;
	}

	public void setClutch(int clutch) {
		this.clutch = clutch;
	}

	public String getMatchupHandedness() {
		return matchupHandedness;
	}

	public void setMatchupHandedness(String matchupHandedness) {
		this.matchupHandedness = matchupHandedness;
	}

	public int getMatchupQualifier() {
		return matchupQualifier;
	}

	public void setMatchupQualifier(int matchupQualifier) {
		this.matchupQualifier = matchupQualifier;
	}

	public int getChartStrikeout() {
		return chartStrikeout;
	}

	public void setChartStrikeout(int chartStrikeout) {
		this.chartStrikeout = chartStrikeout;
	}

	public int getChartGroundball() {
		return chartGroundball;
	}

	public void setChartGroundball(int chartGroundball) {
		this.chartGroundball = chartGroundball;
	}

	public int getChartFlyball() {
		return chartFlyball;
	}

	public void setChartFlyball(int chartFlyball) {
		this.chartFlyball = chartFlyball;
	}

	public int getChartWalk() {
		return chartWalk;
	}

	public void setChartWalk(int chartWalk) {
		this.chartWalk = chartWalk;
	}

	public int getChartSingle() {
		return chartSingle;
	}

	public void setChartSingle(int chartSingle) {
		this.chartSingle = chartSingle;
	}

	public int getChartDouble() {
		return chartDouble;
	}

	public void setChartDouble(int chartDouble) {
		this.chartDouble = chartDouble;
	}

	public int getChartTriple() {
		return chartTriple;
	}

	public void setChartTriple(int chartTriple) {
		this.chartTriple = chartTriple;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

}

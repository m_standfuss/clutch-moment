package com.clutchmoment.data.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stadium_card")
public class StadiumCard implements DataEntity {

	@Column(name = "card_id")
	@Id
	private long cardId;

	@Column(name = "left_field_dimension")
	private int leftFieldDimension;

	@Column(name = "center_field_dimension")
	private int centerFieldDimension;

	@Column(name = "right_field_dimension")
	private int rightFieldDimension;

	@Column(name = "indoor_ind")
	private boolean indoorInd;

	@Column(name = "effect_class")
	private String effectClass;

	@Column(name = "updt_dttm")
	private Date updtDttm;

	@Column(name = "active_ind")
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return getCardId();
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public int getLeftFieldDimension() {
		return leftFieldDimension;
	}

	public void setLeftFieldDimension(int leftFieldDimension) {
		this.leftFieldDimension = leftFieldDimension;
	}

	public int getCenterFieldDimension() {
		return centerFieldDimension;
	}

	public void setCenterFieldDimension(int centerFieldDimension) {
		this.centerFieldDimension = centerFieldDimension;
	}

	public int getRightFieldDimension() {
		return rightFieldDimension;
	}

	public void setRightFieldDimension(int rightFieldDimension) {
		this.rightFieldDimension = rightFieldDimension;
	}

	public boolean isIndoorInd() {
		return indoorInd;
	}

	public void setIndoorInd(boolean indoorInd) {
		this.indoorInd = indoorInd;
	}

	public String getEffectClass() {
		return effectClass;
	}

	public void setEffectClass(String effectClass) {
		this.effectClass = effectClass;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}
}

CREATE DATABASE IF NOT EXISTS `clutch_moment`;
USE `clutch_moment`;

-- This creates the default user with the default password.
CREATE USER IF NOT EXISTS 'clutch_moment_worker'@'%' IDENTIFIED BY 'd94(X{qpEt[Cug-@aHq23';
GRANT ALL ON clutch_moment.* TO 'clutch_moment_worker'@'%' identified by 'd94(X{qpEt[Cug-@aHq23';
FLUSH PRIVILEGES;

--
-- Table structure for table `user`
--
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
	`user_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(45) NOT NULL,
	`email` VARCHAR(100) NOT NULL DEFAULT '',
	`first_name` VARCHAR(50) NOT NULL DEFAULT '', 
	`last_name` VARCHAR(50) NOT NULL DEFAULT '',
	`name_full_formatted` VARCHAR(255) NOT NULL,
	`updt_dttm` DATETIME NOT NULL DEFAULT NOW(),
	`active_ind` TINYINT(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `user_card`
--
DROP TABLE IF EXISTS `user_card`;
CREATE TABLE `user_card` (
	`user_card_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	`card_id` BIGINT(20) UNSIGNED NOT NULL,
	`aquired_dt_tm` DATETIME NOT NULL DEFAULT NOW(),
	`updt_dttm` DATETIME NOT NULL DEFAULT NOW(),
	`active_ind` TINYINT(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`user_card_id`),
	CONSTRAINT `fk_usercard_user_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE,
	CONSTRAINT `fk_usercard_card_cardid` FOREIGN KEY (`card_id`) REFERENCES `card` (`card_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `card`
--
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card` (
	`card_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(45) NOT NULL,
	`card_number` VARCHAR(100) NOT NULL,
	`rarity` VARCHAR(25) NOT NULL, 
	`card_type` VARCHAR(25) NOT NULL, 
	`updt_dttm` DATETIME NOT NULL DEFAULT NOW(),
	`active_ind` TINYINT(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`card_id`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `player_card`
--
DROP TABLE IF EXISTS `player_card`;
CREATE TABLE `player_card` (
	`card_id` BIGINT(20) UNSIGNED NOT NULL,
	`position` VARCHAR(5) NOT NULL,
	`team` VARCHAR(5) NOT NULL,
	`handedness` VARCHAR(5) NOT NULL,
	`salary` SMALLINT(4) NOT NULL,
	`defense` TINYINT(2) UNSIGNED NOT NULL,
	`clutch` TINYINT(2) SIGNED NOT NULL, #Clutch can be negative
	`matchup_handedness` VARCHAR(5) NOT NULL,
	`matchup_qualifier` TINYINT(2) SIGNED NOT NULL, #matchup can be negative
	`chart_strikeout` TINYINT(2) UNSIGNED NOT NULL,
	`chart_groundball` TINYINT(2) UNSIGNED NOT NULL,
	`chart_flyball` TINYINT(2) UNSIGNED NOT NULL,
	`chart_walk` TINYINT(2) UNSIGNED NOT NULL,
	`chart_single` TINYINT(2) UNSIGNED NOT NULL,
	`chart_double` TINYINT(2) UNSIGNED NOT NULL,
	`chart_triple` TINYINT(2) UNSIGNED NOT NULL,
	`updt_dttm` DATETIME NOT NULL DEFAULT NOW(),
	`active_ind` TINYINT(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`card_id`),
	CONSTRAINT `fk_playercard_card_cardid` FOREIGN KEY (`card_id`) REFERENCES `card` (`card_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `card_icon`
--
DROP TABLE IF EXISTS `card_icon`;
CREATE TABLE `card_icon` (
	`card_icon_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`card_id` BIGINT(20) UNSIGNED NOT NULL,
	`icon` VARCHAR(5) NOT NULL,
	`updt_dttm` DATETIME NOT NULL DEFAULT NOW(),
	`active_ind` TINYINT(1) NOT NULL DEFAULT b'1',
	CONSTRAINT `fk_cardicon_card_cardid` FOREIGN KEY (`card_id`) REFERENCES `card` (`card_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `batter_card`
--
DROP TABLE IF EXISTS `batter_card`;
CREATE TABLE `batter_card` (
	`card_id` BIGINT(20) UNSIGNED NOT NULL,
	`on_base` TINYINT(2) UNSIGNED NOT NULL, 
	`speed` TINYINT(2) UNSIGNED NOT NULL,
	`updt_dttm` DATETIME NOT NULL DEFAULT NOW(),
	`active_ind` TINYINT(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`card_id`),
	CONSTRAINT `fk_battercard_card_cardid` FOREIGN KEY (`card_id`) REFERENCES `card` (`card_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `pitcher_card`
--
DROP TABLE IF EXISTS `pitcher_card`;
CREATE TABLE `pitcher_card` (
	`card_id` BIGINT(20) UNSIGNED NOT NULL,
	`command` TINYINT(2) UNSIGNED NOT NULL, 
	`inning_limit` TINYINT(2) UNSIGNED NOT NULL, 
	`mistake_pitch` TINYINT(2) UNSIGNED NOT NULL, 
	`updt_dttm` DATETIME NOT NULL DEFAULT NOW(),
	`active_ind` TINYINT(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`card_id`),
	CONSTRAINT `fk_pitchercard_card_cardid` FOREIGN KEY (`card_id`) REFERENCES `card` (`card_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `stadium_card`
--
DROP TABLE IF EXISTS `stadium_card`;
CREATE TABLE `stadium_card` (
	`card_id` BIGINT(20) UNSIGNED NOT NULL,
	`left_field_dimension` SMALLINT(2) UNSIGNED NOT NULL, 
	`center_field_dimension` SMALLINT(2) UNSIGNED NOT NULL, 
	`right_field_dimension` SMALLINT(2) UNSIGNED NOT NULL, 
	`indoor_ind` TINYINT(1) NOT NULL DEFAULT b'0',
	`effect_class` VARCHAR(100) UNSIGNED NOT NULL, 
	`updt_dttm` DATETIME NOT NULL DEFAULT NOW(),
	`active_ind` TINYINT(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`card_id`),
	CONSTRAINT `fk_stadiumcard_card_cardid` FOREIGN KEY (`card_id`) REFERENCES `card` (`card_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `strategy_card`
--
DROP TABLE IF EXISTS `strategy_card`;
CREATE TABLE `strategy_card` (
	`card_id` BIGINT(20) UNSIGNED NOT NULL,
	`updt_dttm` DATETIME NOT NULL DEFAULT NOW(),
	`active_ind` TINYINT(1) NOT NULL DEFAULT b'1',
	PRIMARY KEY (`card_id`),
	CONSTRAINT `fk_strategycard_card_cardid` FOREIGN KEY (`card_id`) REFERENCES `card` (`card_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


package com.clutchmoment.biz.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.junit.Test;

import com.clutchmoment.biz.card.Batter;
import com.clutchmoment.biz.card.Pitcher;
import com.clutchmoment.biz.card.Player;
import com.clutchmoment.biz.card.Stadium;
import com.clutchmoment.biz.enums.Handedness;
import com.clutchmoment.biz.enums.LineupPosition;
import com.clutchmoment.biz.game.effect.BatterOnBaseClutchEffect;
import com.clutchmoment.biz.game.effect.BatterOnBaseEffect;
import com.clutchmoment.biz.game.effect.Effect;
import com.clutchmoment.biz.game.effect.MistakePitchEffect;
import com.clutchmoment.biz.game.effect.PitcherCommandClutchEffect;
import com.clutchmoment.biz.game.effect.PitcherCommandEffect;
import com.clutchmoment.biz.game.effect.PitcherInningsEffect;
import com.clutchmoment.biz.game.effect.PitcherPitchClutchEffect;
import com.clutchmoment.biz.game.effect.PitcherPitchEffect;
import com.clutchmoment.biz.game.player.BattingOrder;
import com.clutchmoment.biz.game.player.GameCompetitor;
import com.clutchmoment.biz.game.player.Lineup;
import com.clutchmoment.biz.game.player.PlayerDeck;

public class GameTest {

	private static final int REGULAR_DICE_ROLL = 1;

	private static final long USER_ID = 1;
	private static final long GAME_ID = 2;

	@Test
	public void startGame_HappyPath_GameIsStartedOnScorecard() {
		Game game = makeAndStartGame();
		assertTrue("The game is expected to be started.", game.getScorecard().isGameStarted());
	}

	@Test(expected = IllegalArgumentException.class)
	public void startGame_NonFullHomeLineup_ExceptionThrown() {
		Game game = makeGame();
		game.startGame(new Lineup(), makeLineup());
	}

	@Test(expected = IllegalArgumentException.class)
	public void startGame_NonFullAwayLineup_ExceptionThrown() {
		Game game = makeGame();
		game.startGame(makeLineup(), new Lineup());
	}

	@Test
	public void initiatePitch_BeforePreviousAtBatCompleted_ExceptionThrown() {
		Game game = makeAndStartGame();
		game.initiatePitch();
		try {
			game.initiatePitch();
			assertFalse("Should not be able to initiate a pitch before last at bat completed.", true);
		} catch (IllegalStateException e) {
			// We are good.
		}

		game.calculateAdvantage();
		try {
			game.initiatePitch();
			assertFalse("Should not be able to initiate a pitch before last at bat completed.", true);
		} catch (IllegalStateException e) {
			// We are good.
		}

		game.initiateSwing();
		try {
			game.initiatePitch();
			assertFalse("Should not be able to initiate a pitch before last at bat completed.", true);
		} catch (IllegalStateException e) {
			// We are good.
		}

		game.finalizeSwingResult();
		try {
			game.initiatePitch();
			assertFalse("Should not be able to initiate a pitch before last at bat completed.", true);
		} catch (IllegalStateException e) {
			// We are good.
		}

		game.finalizeAtBat();
		/*
		 * After previous at bat is marked as completed we should be able to initiate
		 * the pitch again.
		 */
		game.initiatePitch();
	}

	@Test
	public void initiatePitcher_HappyPath_ReturnsRegularDiceRoll() {
		Game game = makeAndStartGame();
		assertEquals(REGULAR_DICE_ROLL, game.initiatePitch());
	}

	@Test
	public void initiatePitch_PitcherMatchupHandednessPositive_AddsToDiceRoll() {
		Lineup homeLineup = makeLineup(makePitcher(Handedness.RIGHT, 1));
		Lineup awayLineup = makeLineup(makeBatter(Handedness.RIGHT));
		Game game = makeAndStartGame(homeLineup, awayLineup);
		assertEquals(REGULAR_DICE_ROLL + 1, game.initiatePitch());
	}

	@Test
	public void initiatePitch_PitcherMatchupDoesntMatch_NotAddedToDiceRoll() {
		Lineup homeLineup = makeLineup(makePitcher(Handedness.LEFT, 1));
		Lineup awayLineup = makeLineup(makeBatter(Handedness.RIGHT));
		Game game = makeAndStartGame(homeLineup, awayLineup);
		assertEquals(REGULAR_DICE_ROLL, game.initiatePitch());
	}

	@Test
	public void initiatePitch_PitcherPitchInEffect_DiceRollModified() {
		Game game = makeAndStartGame(makeStadium("com.clutchmoment.biz.game.GameTest$MockPitcherPitchEffect"));
		assertEquals(REGULAR_DICE_ROLL + 1, game.initiatePitch());
	}

	@Test
	public void initiatePitch_PitcherPitchClutchInEffect_DiceRollModified() {
		// Positive Clutch
		Lineup homeLineup = makeLineup(makePitcher(1));
		Game game = makeAndStartGame(makeStadium("com.clutchmoment.biz.game.GameTest$MockPitcherPitchClutchEffect"),
				homeLineup, makeLineup());
		assertEquals(REGULAR_DICE_ROLL + 1, game.initiatePitch());

		// Negative Clutch
		homeLineup = makeLineup(makePitcher(-1));
		game = makeAndStartGame(makeStadium("com.clutchmoment.biz.game.GameTest$MockPitcherPitchClutchEffect"),
				homeLineup, makeLineup());
		assertEquals(REGULAR_DICE_ROLL - 1, game.initiatePitch());
	}

	@Test(expected = IllegalStateException.class)
	public void calculateAdvantage_calledBeforePitchInitiated_ExceptionThrown() {
		Game game = makeAndStartGame();
		game.calculateAdvantage();
	}

	@Test(expected = IllegalStateException.class)
	public void calculateAdvantage_CalledAfterAdvantageCalculated_ExceptionThrown() {
		Game game = makeAndStartGame();
		game.initiatePitch();
		game.calculateAdvantage();
		game.calculateAdvantage();
	}

	@Test
	public void calculateAdvantage_HappyPathBatterWinsRoll_AdvantageReturned() {
		Pitcher pitcher = makePitcherWithCommand(5);
		Batter batter = makeBatterWithOnBase(10);
		DiceRoller diceRoller = makeDiceRoller(4);

		Game game = makeAndStartGame(pitcher, batter, diceRoller);
		game.initiatePitch();
		AdvantageResult advantage = game.calculateAdvantage();
		assertFalse("Expecting the batter to have the advantage.", advantage.isPitcherAdvantage());
	}

	@Test
	public void calculateAdvantage_HappyPathRollTied_AdvantageReturned() {
		Pitcher pitcher = makePitcherWithCommand(5);
		Batter batter = makeBatterWithOnBase(10);
		DiceRoller diceRoller = makeDiceRoller(5);

		Game game = makeAndStartGame(pitcher, batter, diceRoller);
		game.initiatePitch();
		AdvantageResult advantage = game.calculateAdvantage();
		assertFalse("Expecting the batter to have the advantage when the scores are tied.",
				advantage.isPitcherAdvantage());
	}

	@Test
	public void calculateAdvantage_HappyPathPitcherWinsRoll_AdvantageReturned() {
		Pitcher pitcher = makePitcherWithCommand(5);
		Batter batter = makeBatterWithOnBase(10);
		DiceRoller diceRoller = makeDiceRoller(6);

		Game game = makeAndStartGame(pitcher, batter, diceRoller);
		game.initiatePitch();
		AdvantageResult advantage = game.calculateAdvantage();
		assertTrue("Expecting the pitcher to have the advantage.", advantage.isPitcherAdvantage());
	}

	@Test
	public void calculateAdvantage_TiredPitcher_DeductedFromCommand() {
		Pitcher pitcher = makePitcherWithMaxInnings(0);
		when(pitcher.getCommand()).thenReturn(5);
		Batter batter = makeBatterWithOnBase(10);
		DiceRoller diceRoller = makeDiceRoller(6);

		Game game = makeAndStartGame(pitcher, batter, diceRoller);
		game.initiatePitch();
		AdvantageResult advantage = game.calculateAdvantage();
		assertFalse("Expecting the pitchers tiredness to be subtracted from command.", advantage.isPitcherAdvantage());
	}

	@Test
	public void calculateAdvantage_TiredPitcherButMaxInningsEffectActive_PitcherCommandNotEffected() {
		Pitcher pitcher = makePitcherWithMaxInnings(0);
		when(pitcher.getCommand()).thenReturn(5);
		Batter batter = makeBatterWithOnBase(10);
		DiceRoller diceRoller = makeDiceRoller(6);

		Game game = makeAndStartGame(makeStadium("com.clutchmoment.biz.game.GameTest$MockPitcherInningsEffect"),
				makeLineup(pitcher), makeLineup(batter), diceRoller);
		game.initiatePitch();
		AdvantageResult advantage = game.calculateAdvantage();
		assertTrue("Expecting the pitchers tiredness not to be subtracted from command.",
				advantage.isPitcherAdvantage());

	}

	@Test
	public void calculateAdvantage_MistakePitchThrown_MistakePitchReturned() {
		Pitcher pitcher = makePitcherWithMistakePitch(1);
		DiceRoller diceRoller = makeDiceRoller(1);

		Game game = makeAndStartGame(pitcher, diceRoller);
		game.initiatePitch();
		AdvantageResult advantage = game.calculateAdvantage();
		assertTrue("Expecting a mistake pitch thrown.", advantage.isMistakePitch());
	}

	@Test
	public void calculateAdvantage_MistakePitchNotThrownButMistakePitchEffectActive_MistakePitchReturned() {
		Pitcher pitcher = makePitcherWithMistakePitch(0);
		DiceRoller diceRoller = makeDiceRoller(1);

		Game game = makeAndStartGame(makeStadium("com.clutchmoment.biz.game.GameTest$MockMistakePitchEffect"), pitcher,
				diceRoller);
		game.initiatePitch();
		AdvantageResult advantage = game.calculateAdvantage();
		assertTrue("Expecting a mistake pitch thrown.", advantage.isMistakePitch());
	}

	@Test
	public void calculateAdvantage_PitcherCommandEffectActive_CommandModified() {
		Pitcher pitcher = makePitcherWithCommand(8);
		when(pitcher.getClutch()).thenReturn(1);
		Lineup homeLineup = makeLineup(pitcher);

		Batter batter = makeBatterWithOnBase(9);
		Lineup awayLineup = makeLineup(batter);

		DiceRoller diceRoller = makeDiceRoller(1);

		Game game = makeAndStartGame(makeStadium("com.clutchmoment.biz.game.GameTest$MockPitcherCommandEffect"),
				homeLineup, awayLineup, diceRoller);
		game.initiatePitch();
		AdvantageResult result = game.calculateAdvantage();
		assertTrue("Expecting the pitcher to have advantage 8 (command) + 1 (roll) + 1 effect = 10 > 9 (onbase)",
				result.isPitcherAdvantage());
	}

	@Test
	public void calculateAdvantage_PitcherCommandClutchEffectActive_CommandModified() {
		Pitcher pitcher = makePitcherWithCommand(8);
		when(pitcher.getClutch()).thenReturn(1);
		Lineup homeLineup = makeLineup(pitcher);

		Batter batter = makeBatterWithOnBase(9);
		Lineup awayLineup = makeLineup(batter);

		DiceRoller diceRoller = makeDiceRoller(1);

		Game game = makeAndStartGame(makeStadium("com.clutchmoment.biz.game.GameTest$MockPitcherCommandClutchEffect"),
				homeLineup, awayLineup, diceRoller);
		game.initiatePitch();
		AdvantageResult result = game.calculateAdvantage();
		assertTrue("Expecting the pitcher to have advantage 8 (command) + 1 (roll) + 1 clutch effect = 10 > 9 (onbase)",
				result.isPitcherAdvantage());
	}

	@Test
	public void calculateAdvantage_BatterHandednessMatchup_MatchupQualifierAddedToOnBase() {
		Pitcher pitcher = makePitcher(Handedness.RIGHT);
		when(pitcher.getCommand()).thenReturn(9);
		Batter batter = makeBatter(Handedness.RIGHT, 1);
		when(batter.getOnBase()).thenReturn(9);

		DiceRoller diceRoller = makeDiceRoller(1);

		Game game = makeAndStartGame(makeLineup(pitcher), makeLineup(batter), diceRoller);
		game.initiatePitch();
		AdvantageResult result = game.calculateAdvantage();
		assertFalse(
				"Expecting the batter to have advantage 9 (onbase) + 1 (handedness matchup) >= 9 (command) + 1 (roll).",
				result.isPitcherAdvantage());
	}

	@Test
	public void calculateAdvantage_BatterHandednessMatchupDoesntMatch_MatchupQualifierNotToOnBase() {
		Pitcher pitcher = makePitcher(Handedness.LEFT);
		when(pitcher.getCommand()).thenReturn(9);
		Batter batter = makeBatter(Handedness.RIGHT, 1);
		when(batter.getOnBase()).thenReturn(9);

		DiceRoller diceRoller = makeDiceRoller(1);

		Game game = makeAndStartGame(makeLineup(pitcher), makeLineup(batter), diceRoller);
		game.initiatePitch();
		AdvantageResult result = game.calculateAdvantage();
		assertTrue("Expecting the pitcher to have advantage 9 (onbase) < 9 (command) + 1 (roll).",
				result.isPitcherAdvantage());
	}

	@Test
	public void calculateAdvantage_BatterOnBaseEffectActive_OnBaseModified() {
		Pitcher pitcher = makePitcherWithCommand(9);
		Batter batter = makeBatterWithOnBase(9);

		DiceRoller diceRoller = makeDiceRoller(1);

		Game game = makeAndStartGame(makeStadium("com.clutchmoment.biz.game.GameTest$MockBatterOnBaseEffect"),
				makeLineup(pitcher), makeLineup(batter), diceRoller);
		game.initiatePitch();
		AdvantageResult result = game.calculateAdvantage();
		assertFalse("Expecting the batter to have advantage 9 (onbase) + 1 (onbase effect) >= 9 (command) + 1 (roll).",
				result.isPitcherAdvantage());
	}

	@Test
	public void calculateAdvantage_BatterOnBaseClutchEffectActive_OnBaseModified() {
		Pitcher pitcher = makePitcherWithCommand(9);
		Batter batter = makeBatterWithOnBase(9);
		when(batter.getClutch()).thenReturn(1);

		DiceRoller diceRoller = makeDiceRoller(1);

		Game game = makeAndStartGame(makeStadium("com.clutchmoment.biz.game.GameTest$MockBatterOnBaseClutchEffect"),
				makeLineup(pitcher), makeLineup(batter), diceRoller);
		game.initiatePitch();
		AdvantageResult result = game.calculateAdvantage();
		assertFalse(
				"Expecting the batter to have advantage 9 (onbase) + 1 (onbase clutch effect) >= 9 (command) + 1 (roll).",
				result.isPitcherAdvantage());
	}

	@Test(expected = IllegalStateException.class)
	public void initiateSwing_LastAdvantageNotCalculated_ExceptionThrown() {
		Game game = makeAndStartGame();
		game.initiateSwing();
	}

	@Test(expected = IllegalStateException.class)
	public void initiateSwing_SwingAlreadyInitiated_ExceptionThrown() {
		Game game = makeAndStartGame();
		game.initiatePitch();
		game.calculateAdvantage();
		game.initiateSwing();
		game.initiateSwing();
	}

	private Game makeAndStartGame() {
		return makeAndStartGame(makeLineup(), makeLineup());
	}

	private Game makeAndStartGame(Stadium homeStadium) {
		return makeAndStartGame(homeStadium, makeLineup(), makeLineup(), makeDiceRoller());
	}

	private Game makeAndStartGame(Pitcher pitcher, DiceRoller diceRoller) {
		Lineup homeLineup = makeLineup(pitcher);
		Lineup awayLineup = makeLineup();
		return makeAndStartGame(homeLineup, awayLineup, diceRoller);
	}

	private Game makeAndStartGame(Pitcher pitcher, Batter batter, DiceRoller diceRoller) {
		Lineup homeLineup = makeLineup(pitcher);
		Lineup awayLineup = makeLineup(batter);
		return makeAndStartGame(homeLineup, awayLineup, diceRoller);
	}

	private Game makeAndStartGame(Lineup homeLineup, Lineup awayLineup) {
		return makeAndStartGame(homeLineup, awayLineup, makeDiceRoller());
	}

	private Game makeAndStartGame(Lineup homeLineup, Lineup awayLineup, DiceRoller diceRoller) {
		return makeAndStartGame(makeStadium(), homeLineup, awayLineup, diceRoller);
	}

	private Game makeAndStartGame(Stadium homeStadium, Lineup homeLineup, Lineup awayLineup) {
		return makeAndStartGame(homeStadium, homeLineup, awayLineup, makeDiceRoller());
	}

	private Game makeAndStartGame(Stadium makeStadium, Pitcher pitcher, DiceRoller diceRoller) {
		return makeAndStartGame(makeStadium, makeLineup(pitcher), makeLineup(), diceRoller);
	}

	private Game makeAndStartGame(Stadium homeStadium, Lineup homeLineup, Lineup awayLineup, DiceRoller diceRoller) {
		Game game = makeGame(homeStadium, diceRoller);
		game.startGame(homeLineup, awayLineup);
		return game;
	}

	private Game makeGame() {
		return makeGame(makeCompetitor(), makeCompetitor(), makeDiceRoller());
	}

	private Game makeGame(Stadium homeStadium) {
		return makeGame(homeStadium, makeDiceRoller());
	}

	private Game makeGame(Stadium homeStadium, DiceRoller diceRoller) {
		return makeGame(makeCompetitor(makeDeck(homeStadium)), makeCompetitor(), diceRoller);
	}

	private Game makeGame(GameCompetitor homeCompetitor, GameCompetitor awayCompetitor, DiceRoller diceRoller) {
		return new Game(homeCompetitor, awayCompetitor, diceRoller);
	}

	private GameCompetitor makeCompetitor() {
		return makeCompetitor(makeDeck());
	}

	private GameCompetitor makeCompetitor(PlayerDeck deck) {
		return new GameCompetitor(USER_ID, deck);
	}

	private Stadium makeStadium() {
		return makeStadium("com.clutchmoment.biz.game.GameTest$MockEffect");
	}

	private Stadium makeStadium(String effectClass) {
		Stadium stadium = mock(Stadium.class);
		when(stadium.getEffectClass()).thenReturn(effectClass);
		return stadium;
	}

	private Lineup makeLineup(Player... players) {
		Lineup lineup = new Lineup();

		Stack<Batter> battersToUse = new Stack<>();
		Pitcher pitcher = null;
		for (int i = players.length - 1; i >= 0; i--) {
			Player player = players[i];
			if (player instanceof Pitcher) {
				pitcher = (Pitcher) player;
			} else {
				battersToUse.push((Batter) player);
			}
		}
		List<Batter> batters = new ArrayList<>();
		for (LineupPosition position : LineupPosition.values()) {
			if (position == LineupPosition.PITCHER) {
				if (pitcher != null) {
					lineup.setPlayer(position, pitcher);
				} else {
					lineup.setPlayer(position, makePitcher());
				}
			} else {
				Batter batter;
				if (battersToUse.isEmpty()) {
					batter = makeBatter();
				} else {
					batter = battersToUse.pop();
				}
				lineup.setPlayer(position, batter);
				batters.add(batter);
			}
		}
		lineup.setBattingOrder(new BattingOrder(batters));
		return lineup;
	}

	private Lineup makeLineup() {
		return makeLineup(new Player[0]);
	}

	private Batter makeBatter(Handedness handednessMatchup, int handednessQualifier) {
		Batter batter = makeBatter();
		when(batter.getMatchupHandedness()).thenReturn(handednessMatchup);
		when(batter.getMatchupQualifier()).thenReturn(handednessQualifier);
		return batter;
	}

	private Batter makeBatter(Handedness handedness) {
		Batter batter = makeBatter();
		when(batter.getHandedness()).thenReturn(handedness);
		return batter;
	}

	private Batter makeBatterWithOnBase(int onBase) {
		Batter batter = makeBatter();
		when(batter.getOnBase()).thenReturn(onBase);
		return batter;
	}

	private Batter makeBatter() {
		Batter batter = mock(Batter.class);
		addRangeOutcomes(batter);
		return batter;
	}

	private Pitcher makePitcher(int clutch) {
		Pitcher pitcher = makePitcher();
		when(pitcher.getClutch()).thenReturn(clutch);
		return pitcher;
	}

	private Pitcher makePitcher(Handedness handednessMatchup, int handednessQualifier) {
		Pitcher pitcher = makePitcher();
		when(pitcher.getMatchupHandedness()).thenReturn(handednessMatchup);
		when(pitcher.getMatchupQualifier()).thenReturn(handednessQualifier);
		return pitcher;
	}

	private Pitcher makePitcherWithCommand(int command) {
		Pitcher pitcher = makePitcher();
		when(pitcher.getCommand()).thenReturn(command);
		return pitcher;
	}

	private Pitcher makePitcherWithMaxInnings(int maxInnings) {
		Pitcher pitcher = makePitcher();
		when(pitcher.getInningsLimit()).thenReturn(maxInnings);
		return pitcher;
	}

	private Pitcher makePitcherWithMistakePitch(int mistakePitch) {
		Pitcher pitcher = makePitcher();
		when(pitcher.getMistakePitchMax()).thenReturn(mistakePitch);
		return pitcher;
	}

	private Pitcher makePitcher(Handedness handedness) {
		Pitcher pitcher = makePitcher();
		when(pitcher.getHandedness()).thenReturn(handedness);
		return pitcher;
	}

	private Pitcher makePitcher() {
		Pitcher pitcher = mock(Pitcher.class);
		when(pitcher.getHandedness()).thenReturn(Handedness.RIGHT);
		when(pitcher.getInningsLimit()).thenReturn(1);
		addRangeOutcomes(pitcher);
		return pitcher;
	}

	private void addRangeOutcomes(Player player) {
		when(player.getResultStrikeout()).thenReturn(1);
		when(player.getResultGroundball()).thenReturn(2);
		when(player.getResultFlyball()).thenReturn(3);
		when(player.getResultWalk()).thenReturn(4);
		when(player.getResultSingle()).thenReturn(5);
		when(player.getResultDouble()).thenReturn(6);
		when(player.getResultTriple()).thenReturn(7);
	}

	private PlayerDeck makeDeck() {
		return makeDeck(makeStadium());
	}

	private PlayerDeck makeDeck(Stadium homeStadium) {
		PlayerDeck deck = new PlayerDeck(new ArrayList<>(), new ArrayList<>(), homeStadium);
		return deck;
	}

	private DiceRoller makeDiceRoller() {
		return makeDiceRoller(REGULAR_DICE_ROLL);
	}

	private DiceRoller makeDiceRoller(int regularRollValue) {
		DiceRoller diceRoller = mock(DiceRoller.class);
		when(diceRoller.getRegularDiceRoll()).thenReturn(regularRollValue);
		return diceRoller;
	}

	public static class MockEffect implements Effect {
		@Override
		public boolean isActiveEffect(GameSituation gameSituation) {
			return false;
		}

	}

	public static class MockPitcherPitchEffect implements Effect, PitcherPitchEffect {
		@Override
		public boolean isActiveEffect(GameSituation gameSituation) {
			return true;
		}

		@Override
		public int modifyPitchersPitchRoll(int currentRoll) {
			return currentRoll + 1;
		}
	}

	public static class MockPitcherPitchClutchEffect implements Effect, PitcherPitchClutchEffect {
		@Override
		public boolean isActiveEffect(GameSituation gameSituation) {
			return true;
		}

	}

	public static class MockPitcherCommandEffect implements Effect, PitcherCommandEffect {
		@Override
		public boolean isActiveEffect(GameSituation gameSituation) {
			return true;

		}

		@Override
		public int modifyPitcherCommand(int currentPitcherCommand) {
			return currentPitcherCommand + 1;
		}
	}

	public static class MockPitcherCommandClutchEffect implements Effect, PitcherCommandClutchEffect {
		@Override
		public boolean isActiveEffect(GameSituation gameSituation) {
			return true;
		}
	}

	public static class MockBatterOnBaseEffect implements Effect, BatterOnBaseEffect {
		@Override
		public boolean isActiveEffect(GameSituation gameSituation) {
			return true;
		}

		@Override
		public int modifyBatterOnBase(int currentOnBase) {
			return currentOnBase + 1;
		}
	}

	public static class MockBatterOnBaseClutchEffect implements Effect, BatterOnBaseClutchEffect {
		@Override
		public boolean isActiveEffect(GameSituation gameSituation) {
			return true;
		}
	}

	public static class MockPitcherInningsEffect implements Effect, PitcherInningsEffect {
		@Override
		public boolean isActiveEffect(GameSituation gameSituation) {
			return true;
		}

		@Override
		public int modifyPitchersMaxInning(int currentMaxInnings) {
			return currentMaxInnings + 1;
		}
	}

	public static class MockMistakePitchEffect implements Effect, MistakePitchEffect {
		@Override
		public boolean isActiveEffect(GameSituation gameSituation) {
			return true;
		}

		@Override
		public int modifyMistakePitchMax(int currentMistakePitchMax) {
			return currentMistakePitchMax + 1;
		}
	}
}
